function [adjmatrix,yids,hierlevel] = getHCAdjMatrix(ytaxa)
%% get adjustancy matrix for hierachical classification and visualization

%% Author: Kirill A. Veselkov, Imperial College London 2017

nSamples = size(ytaxa,1);
nTaxa     = size(ytaxa,2);
yids      = [];
hierlevel = [];
for i = 1: nTaxa
    %yids      = [yids unique(ytaxa(:,i))'];
    hierlevel = [hierlevel i*ones(1,length(unique(ytaxa(:,i))))];
    for j = 1:nSamples
        if length(find(strcmp(ytaxa(j,i),yids))) == 0
            yids = [yids ytaxa(j,i)];
        end
    end
end
nCmpdClasses = length(unique(yids)); index = 1; dubindcs = [];
if nCmpdClasses~=length(yids)
    yidsunique = yids;
    for i = 1:nCmpdClasses
        jTaxonInd = find(cellfun(@(x) strcmp(x,yidsunique{i}),yids));
        if length(jTaxonInd)~=1
            dubindcs(index) = jTaxonInd(1:end-1);
            index        = index + 1;
        end
    end
end

nClasses  = length(yids);
adjmatrix = zeros(nClasses,nClasses);

for iTaxon = 1:nTaxa-1
     yTaxon    = ytaxa(:,iTaxon); % select all samples from a given class
     iTaxonIDs = unique(yTaxon);  nTaxonClasses = length(iTaxonIDs);
     for iTaxonClass = 1:nTaxonClasses 
         iTaxonID    = iTaxonIDs{iTaxonClass};
         iTaxonInd   = cellfun(@(x) strcmp(x,iTaxonID),yids);
         iyTaxaIndcs = cellfun(@(x) strcmp(x,iTaxonID),ytaxa(:,iTaxon));
         jTaxonIDs   = unique(ytaxa(iyTaxaIndcs,iTaxon+1));
         njTaxonClasses = length(jTaxonIDs);
         for jTaxonClass = 1:njTaxonClasses
             jTaxonInd = find(cellfun(@(x) strcmp(x,jTaxonIDs{jTaxonClass}),yids));
             adjmatrix(iTaxonInd,jTaxonInd(end)) = 1;
         end
     end
end
adjmatrix(1:size(adjmatrix,1)+1:end) = 0;
if ~isempty(dubindcs)
    adjmatrix(dubindcs,:) = []; adjmatrix(:,dubindcs)=[];
    yids(dubindcs)        = [];
    hierlevel(dubindcs)   = [];
end
return;