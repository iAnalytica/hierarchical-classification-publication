function hbg = updateNodeColors(hbg)
%% Update colors of node for enhanced visualization of peak annoation module

bgdcolor     = [0.8 0.8 0.8]; %gray
bgdtextcolor = [0 0 0];
selcolor     = [1 0 0];
seltextcolor = [1 1 1];
prscolor     = [0.2 0.6 0.2];
prstextcolor = [1 1 1];
linecolor    = [0 0 0];
nNodes       = length(get(hbg.nodes));
for i = 1:nNodes
    if get(hbg.nodes(i),'isSelected') && get(hbg.nodes(i),'isPresent')
        set(hbg.nodes(i),'Color',selcolor);
        set(hbg.nodes(i),'TextColor',seltextcolor);
        set(hbg.nodes(i),'LineColor',seltextcolor);
    elseif get(hbg.nodes(i),'isPresent')
        set(hbg.nodes(i),'Color',prscolor);
        set(hbg.nodes(i),'TextColor',prstextcolor);
        set(hbg.nodes(i),'LineColor',seltextcolor);
    else
        set(hbg.nodes(i),'Color',bgdcolor);
        set(hbg.nodes(i),'TextColor',bgdtextcolor);
        set(hbg.nodes(i),'LineColor',linecolor);
    end
end
return;