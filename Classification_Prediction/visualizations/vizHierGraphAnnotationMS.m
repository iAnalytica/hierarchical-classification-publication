function vizHierGraphAnnotationMS(ytaxaDB,ytaxaData,yfullids,hmsimage)
%% Visualize 
[adjmat,yids,hierlevel] = getHCAdjMatrix(ytaxaDB);

bg = biograph(adjmat,yids,'LayoutType','hierarchical','ShowArrows','on','NodeAutoSize','on','EdgeType','curved');
set(bg,'AdjMatrix',adjmat); set(bg,'MSInavigator',double(hmsimage));
set(bg,'ytaxa',ytaxaData); set(bg,'nodeHierarchy',hierlevel);
nTaxa = size(ytaxaData,2); yidsData = [];
for i = 1: nTaxa; yidsData = [yidsData unique(ytaxaData(:,i))']; end
%% Present nodes
[ign,presindcs] = intersect(yids,yidsData); 
if size(presindcs,1)>size(presindcs,2)
    presindcs = presindcs';
end
[ign,absindcs] = setdiff(yids,yidsData); 
if size(absindcs,1)>size(absindcs,2)
    absindcs = absindcs';
end


%% Delete this in the future version
nNodes = length(bg.nodes);
maxWidthFullID = 0; 
if ~isempty(yfullids)
    for i = 1:nNodes
        ID = get(bg.nodes(i),'ID');
        index = find(cellfun(@(x) strcmp(x,ID),yfullids(1,:)));
        if ~isempty(index)
            set(bg.nodes(i),'fullID',yfullids{2,index(1)});
            h = text(0,0,yfullids{2,index(1)}); IDpos = get(h,'Extent');  delete(h);
            if IDpos(3)>maxWidthFullID
                maxWidthFullID = IDpos(3);
            end
        else
            set(bg.nodes(i),'fullID',ID);           
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
maxWidthID     = 0;
for i = presindcs
    set(bg.nodes(i),'isPresent',1);
    ID = get(bg.nodes(i),'ID');
    n  = sum(cellfun(@(x) strcmp(x,ID),ytaxaData(:,hierlevel(i))));
    set(bg.nodes(i),'Description',ID);
    set(bg.nodes(i),'ID',sprintf([ID,', n=',num2str(n)]));
    ID = get(bg.nodes(i),'ID');
    h = text(0,0,ID); IDpos = get(h,'Extent');  delete(h);
    if IDpos(3)>maxWidthID
        maxWidthID = IDpos(3);
    end
end
if ~isempty(absindcs)
    for i = absindcs
        set(bg.nodes(i),'isPresent',0);
        ID = get(bg.nodes(i),'ID');
        set(bg.nodes(i),'Description',ID);
        set(bg.nodes(i),'ID',sprintf([ID,', n=',num2str(0)]));
        ID = get(bg.nodes(i),'ID');
        h = text(0,0,ID); IDpos = get(h,'Extent');  delete(h);
    end
end
if maxWidthFullID>maxWidthID
    fontscale = maxWidthFullID./maxWidthID;
else
    fontscale = 1;
end
hbg = viewannot(bg);
set(hbg,'downscale',0);
set(hbg,'fontDownscale',fontscale);
hbg = updateNodeColors(hbg);
hbg.nodes.hgUpdate; 
hbg.hgCorrectFontSize;

end
%set(gcf,'Position',Pos);
%set(hbg.Nodes,'Shape','ellipse');
%set(hbg.Nodes,'Color',msimage.msobject.groupColors(i,:));
%fontsize = cell2mat(get(hbg.Nodes,'FontSize'));
%set(hbg.Nodes,'FontSize',fontsize(1));
%nsize = cell2mat(get(hbg.Nodes,'Size'));
%set(hbg.Nodes,'Size',nsize(1,:)*4);
%g   = biograph.bggui(hbg);

%copyobj(g.biograph.hgAxes,hf);
