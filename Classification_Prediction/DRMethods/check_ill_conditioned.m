function illcond = check_ill_conditioned(X)

[~,d]   = eig(X);
illcond = false;

if(~isreal(d))
    illcond = true;
else
    condNumber = max(diag(d))/min(diag(d));
    if(condNumber > 10)
        illcond = true;
    end
end

end