function plotConfMatDR(confMat,classids,ytaxa, iLevel, fontsize)
%% plotConfMatDR plot confusion matrix
% Input: confMat - confusion matrix 
%        classids - class names
%% Author: Kirill A. Veselkov, Imperial College London, 2012

if nargin<3
    fontsize = 10;
end

nClasses   = length(classids);
accStrings = strtrim(cellstr(num2str(confMat.acc(:),'%0.1f')));
minStrings = strtrim(cellstr(num2str(confMat.min(:),'%0.1f')));
maxStrings = strtrim(cellstr(num2str(confMat.max(:),'%0.1f')));
nCells     = length(accStrings);

%%plot colormaps for previous 2 levels
if iLevel >= 3
    prevLevel = zeros(nClasses-1,1);
    prev2Level = zeros(nClasses-1,1);
for z = 1:(nClasses-1)
    prevLevelidx = find(ytaxa(:,iLevel) == z);
    prevLevel(z,1) = ytaxa(prevLevelidx(1), iLevel-1);
    prev2Level(z,1) = ytaxa(prevLevelidx(1), iLevel-2);
end
prevLevel = [prevLevel; z+1];
colors = distinguishable_colors(length(prevLevel));
prev2Level = [prev2Level; z+1];
colors2 = distinguishable_colors(length(prev2Level));
ax1 = subplot('position',[0.91 0.11 0.01 0.82]);
imagesc(prevLevel);
set(ax1, 'xticklabel', []);
set(ax1, 'xtick', []);
set(ax1, 'yticklabel', []);
set(ax1, 'ytick', []);
colormap(ax1, colors);
freezeColors;

ax3 = subplot('position',[0.92 0.11 0.01 0.82]);
imagesc(prev2Level);
set(ax3, 'xticklabel', []);
set(ax3, 'xtick', []);
set(ax3, 'yticklabel', []);
set(ax3, 'ytick', []);
colormap(ax3, colors2);
freezeColors;
else
end

ax2 = subplot(1,1,1);
imagesc(confMat.acc,[0,100]); 
colormap(ax2, flipud(gray)); 
freezeColors;
ConfPos = getpixelposition(gca);
ButtonSize = 30;
XSpacer = 50;
ButtPosX = ConfPos(1) + ConfPos(3) + ButtonSize + XSpacer;
ButtPosY = ConfPos(2) + ConfPos(4);

% rotateBtn = uicontrol(gcf,...
%     'Style', 'pushbutton',...
%     'CData', msicons.patternrecogn.RotateLabels,...
%     'unit', 'pixels',...
%     'Position', [ButtPosX ButtPosY-ButtonSize ButtonSize ButtonSize],...
%     'CallBack', {@rotateLabels,gca});
% 
% set(rotateBtn, 'UserData', {30});
% set(rotateBtn, 'units','normalized');

for i = 1:nCells
    cellIds{i} = [accStrings{i},'%' char(10) '(', minStrings{i}, '-', maxStrings{i}, '%)'];
end
[x,y]       = meshgrid(1:nClasses); 
hCells      = text(x(:),y(:),cellIds(:), 'HorizontalAlignment','center','FontSize',fontsize);
midValue    = mean(get(gca,'CLim')); 
textColors  = repmat(confMat.acc(:) > midValue,1,3); 
set(hCells,{'Color'},num2cell(textColors,2));  %# Change the text colors
set(gca,'xtick',1:nClasses);
set(gca,'ytick',1:nClasses);
set(gca,'xticklabel',classids,'XAxisLocation','bottom','FontSize',fontsize);
set(gca,'yticklabel',classids,'XAxisLocation','bottom','FontSize',fontsize);
rotateXLabels(gca, 45);
xlabel('Target Class','FontSize',fontsize+2);
ylabel('Predicted Class','FontSize',fontsize+2); 
end

% function rotateLabels(hObject,~,confHandle)
% previousData = get(hObject, 'UserData');
% AngleCount = previousData{1} + 15;
% set(hObject,'UserData', {AngleCount});
% rotateXLabels(confHandle,AngleCount);
% end
