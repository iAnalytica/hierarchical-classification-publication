function [cumulativeConfMat, confMat,ynames, stdev] = getConfMatDR(y,yhat,nonclass,rep, iLevel, varargin)
%% getConfMatDR calculates a confusion matrix
%   Input: y    - class labels
%          yhat - predicted class labels
%% Author: Kirill A.Veselkov, Imperial College London, 2011

[cumulativeConfMat]    = getVarArgin(varargin);

y            = y(:);
yhat         = yhat(:);
yLen         = size(y,1);
[idx,ynames] = grp2idx([y;yhat]);
yidx         = idx(1:yLen);
yhatidx      = idx(yLen+1:yLen*2);
yLen         = size(yidx,1);
ynamesLen    = length(ynames);

confMat.smpls = accumarray([yidx,yhatidx], ones(yLen,1),[ynamesLen, ynamesLen]);
for i = 1:ynamesLen
    confMat.acc(i,:) = 100*confMat.smpls(i,:)./sum(confMat.smpls(i,:));
    if isnan(confMat.acc(i,:))
        confMat.acc(i,:) = 0;
    end
end
ynames = char(ynames);

if nonclass == 0 %if there are no 'non-classified', take the diagonal
    stdev = std(diag(confMat.acc));
    cumulativeConfMat{rep, iLevel} = diag(confMat.acc); %accumulate class accuracies for all prediction repetitions
else
    % if there are 'non-classified' samples, exclude last row and last column from matrix
 newConfMatWithoutNonClass = confMat.acc(1:end-1, 1:end-1);
 stdev = std(diag(newConfMatWithoutNonClass));
 cumulativeConfMat{rep, iLevel} = diag(newConfMatWithoutNonClass); %accumulate class accuracies for all prediction repetitions

end
return;
end

function [cumulativeConfMat] = getVarArgin(argsin)
%% get variable input arguments
cumulativeConfMat  = [];
nArgs = length(argsin);
for i=1:2:nArgs
    switch argsin{i}
        case 'cumMat'
            cumulativeConfMat  = argsin{i+1};
    end
end
end

