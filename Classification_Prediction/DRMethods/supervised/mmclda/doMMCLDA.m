function DRdata = doMMCLDA(DRdata)
% PCA(X) performs  Linear discriminant vectors exctraction based on
% maximum margin criterion
%       uncorrelated features - recursiveMmcLDA.m
%       orthogonal loadings   - mmclda.m
%       Input: DRdata     - various parameters of DR toolbox objects
%              options - default parameters
%   Author: Kirill Veselkov, Imperial College 2012
% shrunk version: Paolo Inglese, 2015

%% setup default parameters
DRdata.options.names{1}  = 'Number of discriminant factors)';
DRdata.options.values{1} = max(2,length(unique(DRdata.groupdata))-1);
DRdata.options.names{2}  = 'Method type: UncorrelatedFactors (UF) or OrthogonalLoadings (OL)';
DRdata.options.values{2} = 'UF';

%% configure method
nDFs         = DRdata.options.values{1};
method       = DRdata.options.values{2};           
%DRdata.meanX = mean(DRdata.X,1);

%% launch MMC-LDA
groups   = unique(DRdata.groupdata);
nGrps    = length(groups);
nSamples = size(DRdata.X, 1);

if (nGrps > 2)
    
    % multiclass - one-vs-all
    DRdata.loadings = cell(nGrps, 1);
    DRdata.scores   = cell(nGrps, 1);
    DRdata.weights  = cell(nGrps, 1);
    DRdata.PCvar    = cell(nGrps, 1);
    for g = 1:nGrps
        
        newy = ones(nSamples, 1);
        newy(DRdata.groupdata == groups(g)) = 2;

        % one-vs-all is always a binary classification
        nDFs = 2;
        
        if strcmp(method, 'UF')
            [DRdata.loadings{g},DRdata.scores{g},DRdata.PCvar{g},DRdata.weights{g}]  = ...
                recursiveMmcLda(DRdata.X,newy,nDFs);
        elseif strcmp(method, 'OL')
            [DRdata.loadings{g},DRdata.scores{g},DRdata.weights{g}] =...
                mmclda(DRdata.X,DRdata.groupdata,nDFs);
            DRdata.PCvar{g}   = [];
        end
        
    end
    
else
    % binary
    if strcmp(method, 'UF')
        [DRdata.loadings,DRdata.scores,DRdata.PCvar,DRdata.weights]  = ...
            recursiveMmcLda(DRdata.X,DRdata.groupdata,nDFs);
    elseif strcmp(method, 'OL')
        [DRdata.loadings,DRdata.scores,DRdata.weights] =...
            mmclda(DRdata.X,DRdata.groupdata,nDFs);
        DRdata.PCvar   = [];
    end
    
end

return;