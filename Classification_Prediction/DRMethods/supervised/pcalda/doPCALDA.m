function DRdata = doPCALDA(DRdata)
% PCA(X) performs  Linear discriminant vectors exctraction based on
% maximum margin criterion
%       uncorrelated features - recursiveMmcLDA.m
%       orthogonal loadings   - mmclda.m
%       Input: DRdata     - various parameters of DR toolbox objects
%              options - default parameters
%   Author: Kirill Veselkov, Imperial College 2012
% shrunk version: Paolo Inglese, 2015

%% setup default parameters
DRdata.options.names{1}  = 'Number of discriminating features';
DRdata.options.values{1} = num2str(max(2,length(unique(DRdata.groupdata))-1));
DRdata.options.names{2}  = 'Number of principal components (use CV to estimate the number of PCs)';
DRdata.options.values{2} = num2str(min(size(DRdata.X))-1); % loseless compression

%% configure method
% DRdata.meanX   = mean(DRdata.X,1);
nDFs           = str2double(DRdata.options.values{1});
nPCs           = str2double(DRdata.options.values{2});

%% launch PCA-LDA
groups   = unique(DRdata.groupdata);
nGrps    = length(groups);
nSamples = size(DRdata.X,1);

if(nGrps > 2)
    
    % multiclass - one-vs-one
    DRdata.loadings = cell(nGrps, 1);
    DRdata.scores   = cell(nGrps, 1);
    DRdata.weights  = cell(nGrps, 1);
    DRdata.PCvar    = cell(nGrps, 1);
    for g = 1:nGrps
        
        newy = ones(nSamples, 1);
        newy(DRdata.groupdata == groups(g)) = 2;
        
        % one-vs-one is always a binary classification
        nDFs = 2;

        [DRdata.loadings{g},DRdata.scores{g},DRdata.PCvar{g},DRdata.weights{g}] = ...
                                                        pcaLda(DRdata.X,newy,nPCs,nDFs);                                      
    end
    
else
    % binary
    [DRdata.loadings,DRdata.scores,DRdata.PCvar,DRdata.weights] = ...
                                                        pcaLda(DRdata.X,DRdata.groupdata,nPCs,nDFs);
end

return;