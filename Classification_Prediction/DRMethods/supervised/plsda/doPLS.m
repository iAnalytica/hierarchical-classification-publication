function DRdata = doPLS(DRdata)
% doPLS  computes partial least squares using simpls or nipals algorithm
%       Input: DRdata     - various parameters of DR toolbox objects
%              options - default parameters
%   Author: Kirill Veselkov, Imperial College 2012
% shrunk version: Paolo Inglese, 2015
    
%% setup default parameters
DRdata.options.names{1}  = 'Number of discriminant factors (e.g. number of classes - 1)';
DRdata.options.values{1} = max(2,length(unique(DRdata.groupdata))-1);
DRdata.options.names{2}  = 'Method type: simpls or nipals';
DRdata.options.values{2} = 'simpls';

%% configure method
method                   = DRdata.options.values{2};
nDFs                     = max(2,length(unique(DRdata.groupdata))-1);
% DRdata.meanX             = mean(DRdata.X,1);

%% launch PLS
groups   = unique(DRdata.groupdata);
nGrps    = length(groups);
nSamples = size(DRdata.X, 1);

if(nGrps > 2)
    % multiclass one-vs-all
    DRdata.loadings = cell(nGrps, 1);
    DRdata.scores   = cell(nGrps, 1);
    DRdata.weights  = cell(nGrps, 1);
    DRdata.PCvar    = cell(nGrps, 1);
    for g = 1:nGrps
        
        newy = ones(nSamples, 1);
        newy(DRdata.groupdata == groups(g)) = 2;
        
        % one-vs-all is always a binary classification
        nDFs = 2;

        if strcmp(method, 'simpls')
            [DRdata.loadings{g},DRdata.scores{g},DRdata.weights{g}, DRdata.PCvar{g}] = ...
                                       plssimplsorig(DRdata.X,getDummyY(newy,'yes'),nDFs, 0);
        %plssimplsorig(getFoldChange(DRdata.X,DRdata.meanX),getDummyY(newy,'yes'),nDFs);
            
        elseif strcmp(method, 'nipals')
            [DRdata.scores{g},DRdata.loadings{g},DRdata.weights{g},DRdata.PCvar{g}] = ...
                                pls2nipals(getFoldChange(DRdata.X,DRdata.meanX),getDummyY(newy,'yes'),nDFs);
                            
        end
        
    end
    
else
    % binary
    if strcmp(method, 'simpls')
        [DRdata.loadings,DRdata.scores,DRdata.weights, DRdata.PCvar] = ...
                    plssimplsorig(DRdata.X,getDummyY(DRdata.groupdata,'yes'),nDFs);
                    %plssimplsorig(getFoldChange(DRdata.X,DRdata.meanX),getDummyY(DRdata.groupdata,'yes'),nDFs);
        
    elseif strcmp(method, 'nipals')
        [DRdata.scores,DRdata.loadings,DRdata.weights,DRdata.PCvar] = ...
                    pls2nipals(getFoldChange(DRdata.X,DRdata.meanX),getDummyY(DRdata.groupdata,'yes'),nDFs);
    end
end

return;