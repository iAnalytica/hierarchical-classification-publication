function [P,T,R,varX,Q,U,varY,B,h] = plssimplsorig(X,Y,ncomp,opt)
%% pls2simpls returns parameters of  the fitted pls regression model derived via NIPALS algorithm...
%% on multiple response variable (Ytrain)

%% Input:
%         Xtrain   - training data set           [samples Xtrain variables in Xtrain]
%         Ytrain   - training data class lebels  [samples Xtrain variables in Ytrain]
%         ncomp    - number of latent components
%         nortcomp - number of orthogonal components to be computed
%
%%  Output:
%          T    - predictive scores of X
%          U    - predictive scores of Y
%          W    - weights of X
%          P    - predictive loadings of X
%          Q    - predictive loadings of Y

if(nargin<4)
    opt = 1; % always center X if opt not passed
end

if(opt==1)
%     X = getFoldChange(X,mean(X,1));
end

[nObs,~] = size(X);
Y        = scaleDataSet(Y,'no','no'); % 'yes','no'

%% Preallocate outputs
% outClass = superiorfloat(X,Y);
% P  = zeros(nVar,ncomp,outClass);
% Q  = zeros(nClasses,ncomp,outClass);
% T  = zeros(nObs,ncomp,outClass);
% U  = zeros(nObs,ncomp,outClass);
% R  = zeros(nVar,ncomp,outClass);

% An orthonormal basis for the span of the X loadings, to make the successive
% deflation X'*Y simple - each new basis vector can be removed from Cov
% separately.
% V    = zeros(nVar,ncomp);
S    = X'*Y; % cross-product
for a = 1:ncomp
    %% SVD on covariance matrix
    [E,D]      = eig(S'*S);
    [~,qidx]   = max(diag(D));
    q          = E(:,qidx); % Y block factor weights
    r          = S*q;       % X block factor weights
    t          = X*r;       % X block factor scores
    t          = bsxfun(@minus, t, mean(t)); % center scores
    normt      = sqrt(t'*t);                 % compute norm
    t          = t ./ normt;                 % normalize scores
    r          = r ./ normt;                 % adapt weights accordingly
    p          = X'*t;                       % X block factor loadings
    q          = Y'*t;                       % Y block factor loadings
    u          = Y*q;                        % Y block factor scores
    v          = p; % initialize orthogonal loadings
    if(a>1)
        v = v - V*(V'*v);
        u = u - T*(T'*u);
    end
    v          = v ./ sqrt(v'*v); % normalize orthogonal loadings
    S          = S - v*(v'*S);    % deflate S with respect to current loadings
    R(:,a) = r;
    T(:,a) = t;
    P(:,a) = p;
    Q(:,a) = q;
    U(:,a) = u;
    V(:,a) = v;
end

B    = R*Q';             % regression coefficients
h    = diag(T*T')+1/nObs;   % leverages of objects
varX = 100 * sum(abs(P).^2,1) ./ sum(sum(abs(X).^2,1));
varY = 100 * sum(abs(Q).^2,1) ./ sum(sum(abs(Y).^2,1));

return;