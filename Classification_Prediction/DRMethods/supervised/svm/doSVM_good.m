function DRdata = doSVM(DRdata)
% SVM: Support Vector Machine analysis using LIBSVM
%
% Ottmar Golf, Kirill Veslekov 2014
% Paolo Inglese (added the multiclass and probability estimation based on
% Platt's scaling) 2015
% shrunk version: Paolo Inglese, 2015

%% setup default parameters
DRdata.options.names{1}  = 'Kernel Type: Linear (0), Polynomial (1), Radial basis function (2) or sigmoid (3)';
DRdata.options.values{1} = num2str(0);
DRdata.options.names{2}  = 'Automated parameter selection: Yes or No';
DRdata.options.values{2} = 'No';
DRdata.options.names{3}  = 'Centering: Yes or No';
DRdata.options.values{3} = 'Yes';

%% Mean centering
if strcmp(DRdata.options.values{3}, 'Yes')
    DRdata.meanX = mean(DRdata.X, 1);
    X            = getFoldChange(DRdata.X, DRdata.meanX);
else
    DRdata.meanX = [];
    X            = DRdata.X;
end

%% Automated parameter selection
if strcmp(DRdata.options.values{2},'Yes') && ~strcmp(DRdata.options.values{1},'0')...
        && ~isfield(DRdata,'bestcv');
    bestcv = 0;
    for log2c = -5:8,
        for log2g = -8:4,
            cmd = ['-q -v 5 -t ' num2str(DRdata.options.values{1}) ' -c ', num2str(2^log2c), ' -g ', num2str(2^log2g)];
            cv = svmtrain(DRdata.groupdata, X, cmd);
            if (cv >= bestcv),
                DRdata.bestcv = cv; DRdata.bestc = 2^log2c; DRdata.bestg = 2^log2g;
            end
            % fprintf('%g %g %g (best c=%g, g=%g, rate=%g)\n', log2c, log2g, cv, bestc, bestg, bestcv);
        end
    end
    cmd = ['-q -t ' num2str(DRdata.options.values{1}) ' -c ', num2str(DRdata.bestc), ' -g ', num2str(DRdata.bestg)];
elseif isfield(DRdata,'bestcv')
    cmd = ['-q -t ' num2str(DRdata.options.values{1}) ' -c ', num2str(DRdata.bestc), ' -g ', num2str(DRdata.bestg)];
else
    cmd = ['-q -t ' num2str(DRdata.options.values{1})];
end

%% SVM analysis
% probability estimation one-vs-all
groups   = unique(DRdata.groupdata);
nGroups  = length(groups);
nSamples = length(DRdata.groupdata);
nVars    = size(DRdata.X, 2);

if nGroups > 2
    %%% multiclass
    DRdata.oneVSall     = true;
    DRdata.nCls         = nGroups;
    DRdata.scores       = cell(nGroups, 1);
    DRdata.weights      = cell(nGroups, 1);
    DRdata.loadings     = cell(nGroups, 1);
    DRdata.bias         = cell(nGroups, 1);
    for g = 1:nGroups
        newLabels          = ones(nSamples, 1);
        newLabels(DRdata.groupdata == groups(g)) = 2;
        model              = svmtrain(newLabels, X, cmd);
        DRdata.bias{g}     = -model.rho;
        DRdata.weights{g}  = (model.sv_coef'*full(model.SVs))';
        DRdata.scores{g}   = X * DRdata.weights{g} + DRdata.bias{g};
        DRdata.loadings{g} = X'* DRdata.scores{g} ./ (DRdata.scores{g}' * DRdata.scores{g});
    end
else
    % binary classification based on orthogonal projection
    DRdata.oneVSall = false;
    DRdata.nCls     = 2;
    DRdata.scores   = zeros(nSamples, DRdata.nCls);
    DRdata.weights  = zeros(nVars, DRdata.nCls);
    DRdata.loadings = zeros(nVars, DRdata.nCls);
    DRdata.bias     = zeros(DRdata.nCls, 1);
    for iD = 1:DRdata.nCls
        
        model               = svmtrain(DRdata.groupdata(:), X, cmd);
        DRdata.bias(iD)     = -model.rho;
        
        w = (model.sv_coef'*full(model.SVs))';
        w = w / norm(w);
        t = X * w + DRdata.bias(iD);
        t = t / sqrt(t'*t);
        p = X' * t;
        
        DRdata.weights(:,iD)  = w;
        DRdata.scores(:,iD)   = t;
        DRdata.loadings(:,iD) = p;
        
        X = X - (X*p)*p';
        
    end
end

end