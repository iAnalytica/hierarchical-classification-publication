function [y_predicted, P] = build_and_predict_after_dr(trainset, Xtest, model)
% build the models and do the prediction
%
% Author: Paolo Inglese, Imperial College, 2017

groups   = unique(trainset.groupdata);
nGrps    = length(groups);
nSamples = length(trainset.groupdata);

if (nGrps > 2)
    % multiclass - one-vs-all
    P   = zeros(size(Xtest,1), nGrps);
    yid = zeros(size(Xtest,1), 1);
    for g = 1:nGrps
       
        % SVM needs the bias term and uses only the first dimension for the
        % scores and weights - the second dimension is just for
        % visualisation purposes
        dims = 1:size(model.scores{g}, 2);
        bias = 0;
        if(strcmp(trainset.method, 'SVM'))
            dims = 1;
            bias = model.bias{g}(:,dims);
        end
        
        newy = ones(nSamples, 1);
        newy(trainset.groupdata == groups(g)) = 2;
        model.B{g} = mnrfit(model.scores{g}(:,dims), newy);
        
        testScores = Xtest * model.weights{g}(:,dims) + bias;
        Ptmp = mnrval(model.B{g},  testScores);
        P(:,g) = Ptmp(:,2);
        
        yid(g) = groups(g);
        
    end
    
    y_predicted = probToClass(P, yid);
    
else
    
    % binary
    
    % SVM needs the bias term and uses only the first dimension for the
    % scores and weights - the second dimension is just for
    % visualisation purposes
    dims = 1:size(model.scores, 2);
    bias = 0;
    if(strcmp(trainset.method, 'SVM'))
        dims = 1;
        bias = model.bias(dims);
    end
    
    model.B = mnrfit(model.scores(:,dims), trainset.groupdata);
    
    testScores = Xtest * model.weights(:,dims) + bias;
    P = mnrval(model.B,  testScores);
    y_predicted = probToClass(P, groups);
    
end
end