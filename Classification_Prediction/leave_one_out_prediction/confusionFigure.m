function [ output_args ] = confusionFigure( cmat,labs, index )
%confusionFigure - draw a confusion matrix in a nice way

% Define the default colour scheme
col = 0.6;

% Size of cmat
sz = size(cmat);

% Create empty image
img = zeros(sz(1),sz(2),3);

% Scale cmat to be a percentage (sum over each row)
scm = sum(cmat,2);

cper = round(1000 * bsxfun(@rdivide,cmat,scm)) / 10;

% Add in the colours as necessary...
for r = 1:sz(1)
    for c = 1:sz(2)
        img(r,c,:) = [col cper(r,c)/100 1];
    end
end

img = hsv2rgb(img);

% Draw figure
f = figure('Position',[200 200 900 900]); hold on;

% Get image axes
ax = imagesc(img);

% Put in the non-zero values
for r = 1:sz(1)
    for c = 1:sz(2)
        if cmat(r,c) ~= 0
            val = cmat(r,c);
            per = cper(r,c);
            text(c,r,[num2str(per) '%' char(10) '(' int2str(val) ')'],...
                'FontSize',20,...
                'HorizontalAlignment','center');
        else
            text(c,r,'-','HorizontalAlignment','center');
        end
    end
    newlabs{r,1} = [labs{r,1} ' - ' int2str(r)];
    ylabsnew{r,1} = int2str(r);
end
ylabsnew{r+1,1} = 'Unclassified';

% Format the axes and labels
set(gca,'YDir','reverse',...
    'XAxisLocation','top',...
    'XTick',1:sz(2),...
    'YTick',1:sz(1),...
    'YTickLabel',newlabs,...
    'XTickLabel',ylabsnew);

ylabel('Actual','FontSize',20','FontWeight','bold');
xlabel('Predicted','FontSize',20','FontWeight','bold');

% Sizes
set(gca,'FontSize',20);

xlim([0.5 sz(2)+0.5]);

% Save the figure
graphFormat([pwd filesep 'A22-HierCmat-' int2str(index)],'png');


end

