function [yTest,Ptest,yTrain,yids,yidsIndcs,nodes,ct] = doHierTrainTest(XTrain,XTest,TrueY,ClassTreeDir,options)
%% Hierachical analysis of train and test set data for MSI data
%   TrueY being the histological IDs of the train set in a cell vector

% Get ClassTree data
exelPath = [ClassTreeDir 'ClassTree.xlsx'];
[ct]     = getClassTree(exelPath);

% Optional visualisation of tree
% graphHier(ct)

% Get hierachical TrueY
TrueYHier = TrueY1DtoTrueYHier(ct,TrueY);

% Get options right
[opts,method] = optsToDRoptions(options);

% Train the model
tdmodel  = topdownHCtrain(XTrain,TrueYHier,'method',method,'options',opts);

% Predict using the unseen variables...
[yTest,Ptest] = topdownHCpredict(XTest,tdmodel);

% get IDs
[yTrain,yids,nodes] = getNodeHierarchies(TrueYHier);
yidsIndcs = tdmodel.yid;
% integrate node of first level
[nNodes,nLvls]   = size(nodes.yid);
nodeIDs          = cell(nNodes,nLvls+1);
nodeIDs(:,2:end) = nodes.yid;
nodeIDs{1,1}     = yids{1,1};
nodes.yid        = nodeIDs;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [cl] = getClassTree(xlp)
% Read / interpret the class tree in the excel file so that it is suitable
% for use in the function

% Read in the file
[~,~,raw] = xlsread(xlp);

% Divide into level description and colors
nCols      = size(raw,2); 
midCol     = floor(nCols/2);
LvlCols    = 1:midCol;
ColourCols = midCol+2:nCols;
AltIDs     = ceil(nCols/2);

% First row is the labels
cl.labs = raw(1,1:end);

% Second onwards are the actual classifications
cl.tree = raw(2:end,LvlCols);

% These are the alternative names for the accepted classification
cl.alt = raw(2:end,AltIDs);

% Get the colours
cl.colours = raw(2:end,ColourCols);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
