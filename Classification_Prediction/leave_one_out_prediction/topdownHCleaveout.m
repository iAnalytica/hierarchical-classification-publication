function topdownHCleaveout(X,ytaxa, nReps, varargin)
%%%% Input: X - expression matrix (m x n) where m = samples and n = features
%%%%		ytaxa - class assignment where each sample should have a corresponding
%%%%				row label. Columns indicate the tree levels. A 200 x 5 matrix
%%%%				means 200 samples with 5 hierarchy levels.
%%%%		nReps - number of repetitions to carry out predictions 

%% close any previous figures
close;

%% remove root label (column) if it exists
if strcmpi(ytaxa(1), 'root');
    ytaxa = ytaxa(:, 2:end);
else
end

%% convert label strings to integer
for c = 1:size(ytaxa, 2)
    ytaxaConv(:, c) = grp2idx(ytaxa(:, c));
end

%% determine unique classes at bottom level
uniqueSpecies 	= unique(ytaxaConv(:, end));
speciesList 	= unique(ytaxa(:,end));
nBottomClasses 	= length(uniqueSpecies);

%% create cell to hold class predictions (later)
yPred 		= cell(numel(uniqueSpecies), 1);
predSpecies = cell(1, numel(uniqueSpecies));

%% allow user to define which class/species to be excluded from training set and predicted
prompt 	= {'Enter genus name to leave out:', 'Enter corresponding species name:'};
dlg_title 	= 'Enter species details';
num_lines 	= 1;
def 		= {'Enterobacter', 'cloacae'};
answer		= inputdlg(prompt,dlg_title,num_lines,def);
LOOspStr 	= [answer{1}, ' ', answer{2}];

%% find indices for user-defined species
LOOidx 	= find(strcmp(ytaxa(:,end), LOOspStr),1);
LOOsp 		= ytaxaConv(LOOidx,end);

nLevels 	= size(ytaxa,2);
interestID1 = zeros(nLevels, 1);

%% clear variables not used anymore
clear dlg_title num_lines def answer LOOspStr

%% start excluding and predicting one class at a time
for LOOspNum = LOOsp
    
    %% create matrix to hold overall prediction accuracies at the different hierarchy 
    %% levels
    cvaccuracyStacked = zeros(nReps, nLevels);
    
    %% repeat each prediction 'nReps times'
    for rep = 1:nReps
    
        %% find LOO class indices and set as test indices
        idxTest = find(ytaxaConv(:,end) == uniqueSpecies(LOOspNum));
        idxTestBool = ytaxaConv(:, end) == uniqueSpecies(LOOspNum);

        %% set all other classes (i.e. not LOO class) as training indices
        idxTrain = find(ytaxaConv(:, end) ~= uniqueSpecies(LOOspNum));
        
        %% split ytaxa data for training and test
        trainYtaxa 		= ytaxa(idxTrain,:);
        trainYtaxaConv 	= ytaxaConv(idxTrain,:);
        
        %% split X data for training and test, and mean-center
        meanX 			= mean(X(idxTrain,:));
        trainX 			= getFoldChange(X(idxTrain,:),meanX);
        testX 			= getFoldChange(X(idxTest,:),meanX);
        
        %% draw missing classification tree
        [adjM,yids,~] 	= getHCAdjMatrix([repmat({'root'}, size(trainYtaxa, 1), 1), trainYtaxa]);
        bg1 		  	= biograph(adjM,yids,'LayoutType','hierarchical','ShowTextInNode', 'Label', 'ArrowSize', 0);
        hBio 		  	= view(bg1);
        
        %% show dialog box with the predicting class/species
        LOOspStr 		= ytaxa(idxTest(1,:),:);
        LooSpMsg 		= msgbox(sprintf('Predicting Species: %s', LOOspStr{1,end}));
        movegui(LooSpMsg,'northeast');
        
        %% train the hierarchical model
        tdmodel     	= topdownHCtrain(trainX, trainYtaxa, ytaxa, speciesList, LOOspStr, LOOspNum, 'biograph', hBio);
        
        %% determine ID of interest
        for iLevel = 1:nLevels
            if iLevel > 1
%                 ParentNodeID 		= unique(ytaxaConv(idxTest, iLevel-1)); %% gets the parentnode ID
%                 childIdx 			= ytaxaConv(:,iLevel-1) == ParentNodeID;
%                 ofInterestIdx 		= idxTestBool.*childIdx;
%                 childIdx 			= find(ytaxaConv(:,iLevel-1) == ParentNodeID);
%                 [allIDs,~,b] 		= grp2idx(unique(ytaxaConv(childIdx, iLevel)));
%                 interestID1(iLevel) = allIDs(find(b == unique(ytaxaConv(logical(ofInterestIdx),iLevel))));
                interestID1(iLevel) = unique(ytaxaConv(idxTest,iLevel));%ytaxaConv(ytaxaConv(:,iLevel-1) == ParentNodeID, iLevel);
            else
%                 ofInterestIdx 		= logical(idxTestBool.*ytaxaConv(:,iLevel));
%                 [allIDs,~,b] 		= grp2idx(unique(ytaxaConv(:,iLevel)));
%                 interestID1(iLevel) = find(b == unique(ytaxaConv(ofInterestIdx)));
                  interestID1(iLevel) = unique(ytaxaConv(idxTest,iLevel));
            end
        end
        
        %% test the hierarchical model
        [yPred{LOOspNum},~, LOOscores] = topdownHCpredict(testX,tdmodel, interestID1);
        
        newLOOscores 	= cell(nLevels,1);
        
        %% create matrix to hold cross-validation accuracies for current repetition
        cvaccuracy 		= zeros(1,nLevels);
        fprintf('accuracy: \n');
        
        %% calculate prediction accuracies for all levels except the last (because all 
        %% samples for class of interest were excluded from the bottom-most level
        for iLevel = 1:nLevels-1
            
            %% calculate percentage accuracy for species of interest for iLevel
            trainClassesConv = getNodeHierarchies(ytaxa(idxTrain,:));
            trainClasses = ytaxa(idxTrain,:);
            predStats = tabulate(yPred{LOOspNum}(:,iLevel));
            predictedClasses = cell(size(predStats,1),1);
            predictionAccuracy = 0;
            
            for iPred = 1:size(predStats,1)
                predictedClasses{iPred} = unique(trainClasses(trainClassesConv(:,iLevel) == predStats(iPred,1),iLevel));
                boolMatch = strcmp(unique(ytaxa(idxTest,iLevel)),predictedClasses{iPred}(:));
                if boolMatch
                    predictionAccuracy = predStats(iPred,3);
                end
            end
            
            cvaccuracy(iLevel) = predictionAccuracy;%100*sum(yPred{LOOspNum}(:,iLevel) == ytaxaConv(idxTest,iLevel))./length(idxTest);
            
%             for ss = 1:length(yPred{LOOspNum})
%                 indexVal = yPred{LOOspNum}(ss,iLevel);
%                 if isnan(indexVal)
%                     predSpecies{1,LOOspNum}(ss, iLevel) = cellstr('non-classified');
%                 else
%                     predspeciesIdx 						= find(ytaxaConv(:, iLevel) == indexVal);
%                     predSpecies{1,LOOspNum}(ss,iLevel) 	= ytaxa(predspeciesIdx(1), iLevel);
%                 end
%             end
            
            if iLevel > 1
                ParentNodeID 	= unique(ytaxaConv(idxTest, iLevel-1)); %% gets the parentnode ID
                SisterNodesIdx 	= find(trainYtaxaConv(:, iLevel-1) == ParentNodeID);
                SisterNodesX 	= trainX(SisterNodesIdx,:);
                SisterNodesY 	= trainYtaxaConv(SisterNodesIdx,:);
                SisterNodesYStr = trainYtaxa(SisterNodesIdx,:);
                childIdx 		= ytaxaConv(:,iLevel-1) == ParentNodeID;
                ofInterestIdx 	= idxTestBool.*childIdx;
                childIdx 		= find(ytaxaConv(:,iLevel-1) == ParentNodeID);
                [allIDs,~,b] 	= grp2idx(unique(ytaxaConv(childIdx, iLevel)));
                interestID 		= allIDs(find(b == unique(ytaxaConv(logical(ofInterestIdx),iLevel))));
                %%%interestID = unique(ytaxaConv(idxTest,iLevel));%%%%%%%%%
            else
                SisterNodesX 	= trainX;
                SisterNodesY 	= trainYtaxaConv;
                SisterNodesYStr = trainYtaxa;
                ofInterestIdx 	= logical(idxTestBool.*ytaxaConv(:,iLevel));
                [allIDs,~,b] 	= grp2idx(unique(ytaxaConv(:,iLevel)));
                interestID 		= find(b == unique(ytaxaConv(ofInterestIdx)));
                %%%interestID = unique(ytaxaConv(idxTest,iLevel)); %%%%%%%%%
            end
           
            if isempty(testX) || isempty(SisterNodesX)
                break;
            end
            
            trainClasses 	= unique(SisterNodesY(:, iLevel), 'stable');
            numTrainClasses = length(trainClasses);
            
            if iLevel > 1
                %% Determine good predictions in the previous level
                goodPredictionsIdx = find(yPred{LOOspNum}(:,iLevel-1) == unique(ytaxaConv(idxTest,iLevel-1)));
                %%% How many errors were propagated from previous levels?
                CumError{iLevel} = length(yPred{LOOspNum}(:,iLevel-1)) - length(goodPredictionsIdx);
                newLOOscores{iLevel} = NaN(length(idxTest),2);
                %%% If problem is not binary, take one set of scores
                %%% from the model with the expected class-vs-all
                if isempty(goodPredictionsIdx)
                    break
                end
                if iscell(LOOscores{iLevel})
                    newLOOscores{iLevel} = LOOscores{iLevel, ParentNodeID}{interestID};
                else
                    newLOOscores{iLevel} = LOOscores{iLevel};
                end
            else
                CumError{1} = 0;
                newLOOscores{1} = LOOscores{1};
            end
            
            hS = figure;
            txtClasses = unique(SisterNodesYStr(:, iLevel), 'stable');
            idxLegend = 0;
            legendTxt = [];
            idxNonClass = find(isnan(yPred{LOOspNum}(:,iLevel)));
            if iLevel > 1
                prevNonClass = find(isnan(yPred{LOOspNum}(:,iLevel-1)));
                idxNonClass = idxNonClass(~ismember(idxNonClass, prevNonClass));
            end
            pause(0.1);
            %%%%% PLOT NON-CLASSIFIED SAMPLES
            if ~isempty(idxNonClass)
                plot(newLOOscores{iLevel}(idxNonClass, 1), newLOOscores{iLevel}(idxNonClass, 2), '.', ...
                    'Marker', 'x', ...
                    'MarkerSize', 12, ...
                    'LineWidth', 2, ...
                    'MarkerEdgeColor', 'r');
                idxLegend = idxLegend + 1;
                legendTxt{idxLegend} = ['non-classified'];
            end

            hold on
            
            %% Exclude red and green plot colors (used as a border color indicate correct/wrong prediction)
            ExcludeColors 	= [1 0 0; 0 1 0];
            
            %% Generate 2 distinguishable colors from each other and from red and green
            colors 			= distinguishable_colors(numTrainClasses, ExcludeColors);
            
            for c = 1:numTrainClasses
                
                idxTestClass = find(yPred{LOOspNum}(:, iLevel) == trainClasses(c));
                idxTrainClass = find(SisterNodesY(:, iLevel) == trainClasses(c));
                
                if iLevel == 1
                    ParentNodeID = 1;
                end
                
                if (length(unique(tdmodel.yid{1,iLevel}{ParentNodeID})) <= 2)
                    scoresX = tdmodel.model{1,iLevel}{ParentNodeID}.scores(idxTrainClass,1);
                    scoresY = tdmodel.model{1,iLevel}{ParentNodeID}.scores(idxTrainClass,2);
                else
                    scoresX = tdmodel.model{1,iLevel}{ParentNodeID}.scores{interestID}(idxTrainClass,1);
                    scoresY = tdmodel.model{1,iLevel}{ParentNodeID}.scores{interestID}(idxTrainClass,2); 
                end
                pause(0.1);
                %%% plot training scores (all in same color except class equal to idxTestClass)
                plot(scoresX, scoresY, '.',  ...
                    'Marker', 'd', ...
                    'MarkerSize', 11, ...
                    'MarkerFaceColor', colors(c,:), ...
                    'MarkerEdgeColor', 'k');
                pause(0.1);
                if ~isempty(idxTestClass)
                    if(unique(yPred{LOOspNum}(idxTestClass, iLevel)) == unique(ytaxaConv(idxTest, iLevel)))
                        MarkerEdgeColor = 'g';
                    else
                        MarkerEdgeColor = 'r';
                    end
                    
                    plot(newLOOscores{iLevel}(idxTestClass,1), newLOOscores{iLevel}(idxTestClass,2), '.', ...
                        'Marker', 'o', ...
                        'MarkerSize', 11, ...
                        'LineWidth', 2, ...
                        'MarkerFaceColor', colors(c, :), ...
                        'MarkerEdgeColor', MarkerEdgeColor);
                end
                
                if ~isempty(idxTrainClass)
                    idxLegend = idxLegend + 1;
                    legendTxt{idxLegend} = ['train ', txtClasses{c}];
                end
                
                if ~isempty(idxTestClass)
                    idxLegend = idxLegend + 1;
                    legendTxt{idxLegend} = ['pred ', txtClasses{c}];
                end
            end
            hold on
%             plot([0 0],[0 0],'w-');
            clear c
            box on
            hold off
            
            idxLegend = idxLegend +1 ;
            legendTxt{idxLegend} = [num2str(CumError{iLevel}),' sample(s) propagated error'];
            
            legendArg = [];
            for iLegend = 1:idxLegend
                legendArg = [legendArg, '''', legendTxt{iLegend}, '''', ','];
            end
            h = eval(['legend(', legendArg(1:end-1), ')']);
            set(h, 'FontSize', 13);
            oldWinPos = get(hS, 'Position');
            set(hS, 'Position', [1 50 oldWinPos(3) oldWinPos(4)]);
            predicting_species = ytaxa{idxTest(1), iLevel};
            title_var = sprintf('Predicting: %s  \n  Accuracy: %.2f%%', predicting_species, cvaccuracy(iLevel));
            title(title_var, 'FontSize', 13);
            set(gca, 'FontSize',13);
            xlabel(['Discriminant Component 1']);
            ylabel(['Discriminant Component 2']);
            %pause;
            str = ['plot_', predicting_species, '_level_', num2str(iLevel), '_rep_', num2str(rep)];
            print(gcf,str,'-djpeg', '-r300');
            close;
            fprintf('Level: %d; %f; \n', iLevel, cvaccuracy(iLevel));
            idxLegend = 0;
        end
        
%         %%% save predictions to a text file
%         fprintf('\n');
%         child_handles = allchild(0);
%         names = get(child_handles,'Name');
%         k = strncmp('Biograph Viewer', names, 15);
%         close(child_handles(k));
%         close(LooSpMsg);
%         cvaccuracyStacked(rep,:) = cvaccuracy(:,1:5);
cvaccuracyStacked(rep,:) = cvaccuracy(1:end);
    end
%     file_name = ['75%_Predictions_', LOOspStr{end}, '.txt'];
%     fid = fopen(file_name,'w');
%     
%     for rep = 1:nReps
%         for lev = 1:nLevels-1
%         fprintf(fid, '%f,', cvaccuracyStacked(rep,lev), ' ,');
%         end
%         fprintf(fid, '\n');
%     end    
%     fclose(fid);
filename = ['90%_Predictions_', LOOspStr{end}, '.xlsx'];
xlswrite(filename, cvaccuracyStacked,1,'A1:F5');
close;
end