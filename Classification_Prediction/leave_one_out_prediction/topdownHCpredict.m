function [ytest,P, TestScores] = topdownHCpredict(Xtest, tdmodel, interestID)
%% topdownHCpredict performs training of supervised maching learning algorithms...
%% for hierarchical top-down classification
%   Xtest   - a test set
%   tdmodel - fitted model for hierarchical classificaiton
%% Author: Kirill A. Veselkov, Imperial College London, 2017

%% Prediction phase
nLevels  = length(tdmodel.y); %determine the number of class hierarchies
nObs     = size(Xtest,1);
ytest    = NaN(nObs,nLevels);
P        = cell(1,nLevels);
TestScores        = cell(1,nLevels);
for iLevel = 1:nLevels-1
    nModelsiLevel = length(tdmodel.model{iLevel});
    for jModel = 1:nModelsiLevel
        nClasses         = length(tdmodel.yid{iLevel}{jModel});
        %%%In P each row is the iLevel and each column is the node/model
        P{iLevel,jModel} = NaN(nObs,nClasses);
        TestScores{iLevel} = NaN(nObs,2);
    end
end
obsindcs  = ones(nObs,1)==1;
[ytest,P, TestScores] = topdownHCrecpred(Xtest, tdmodel, ytest, P, 1, 1, obsindcs, TestScores, interestID);
end

function  [ytest,P, TestScores] = topdownHCrecpred(Xtest, tdmodel, ytest, P, iLevel, jNode, obsindcs, TestScores, interestID)
%% using recursive algorithm for topdown classification

%% convergence criterium for recursion
if sum(obsindcs) ==0
    return;
end
currInterestID = interestID(iLevel);
%% fit model at a given level of hierarchy
%%% mean center
m = tdmodel.model{iLevel}{jNode}.meanX;
testX = bsxfun(@minus,Xtest,m);

[ytest(obsindcs,iLevel),P{iLevel,jNode}(obsindcs,:),TestScores{iLevel}(obsindcs,:)] = ...
    multivmodelpredict(testX(obsindcs,:),tdmodel.model{iLevel}{jNode},tdmodel.yid{iLevel}{jNode},currInterestID);
obsindcs               = ~isnan(ytest(:,iLevel)) & obsindcs;

%% convergence criterium for recursion
if iLevel == length(tdmodel.y)-1
    return;
end

nodeids = unique(ytest(obsindcs,iLevel))';
for jNode = nodeids
    obsindcs  = ytest(:,iLevel)==jNode;
    [ytest,P,TestScores] = topdownHCrecpred(Xtest,tdmodel, ytest, P, iLevel+1, jNode, obsindcs, TestScores, interestID);
end
return;
end