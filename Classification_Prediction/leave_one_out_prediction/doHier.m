function [ ytest ] = doHier(src,event,files,fig)
%doHier - J's function for running the various stages of the analysis...

% Get the selected file(s)
[files,fList] = doEligibility(fig,files);

% Read in the data - need the x data, pixel information, class labels...
[data] = getData(fig,files);

% Read in the hierarchical classifcation data
xlp = [pwd filesep 'sourcePackages/ClassTree.xlsx'];
[cl] = getClassTree(xlp);

%graphHier(cl)
%ytest = cl;
%assignin('base','cl',cl)

%return

% Convert the current pixel region annotations in the h5 file from numbers
% to a series of ytaxa-like annotations
[data] = pixelClass2Hier(data,cl);
 
% Train the model
tdmodel  = topdownHCtrain(data.train, data.hier);

% Predict using the unseen variables...
ytest  = topdownHCpredict(data.x, tdmodel);

% Convert the 'true' classifications to the new numeric representation in
% the same format as the output ytest, as this is not the same as the excel
% spreadsheet.
true.orig = data.true;
[true.true,true.ids,true.node] = getNodeHierarchies(data.true);
data.true = true;

% Also need to 'convert' the training set classifications to the same
% format as above, as when a class is omitted the results are currently not
% correctly interpreted in the confusion matrix.
[train.true,train.ids,train.node] = getNodeHierarchies(data.hier);

% % Create the include vector so that we can exclude the training set
% % from the confusion matrix statistics
% tmp = sum(data.anno,2);
% inc = tmp(data.inds,:);
% inc = inc == 0;

% Here do a confusion matrix of true / predicted classifactions. Should do
% one for each of the hierarchies? How would the results of a not-present
% tissue type be identified?
for n = 1:size(data.true.ids,2)
    
    % Do the confusion matrix calculation - may need heavy re-writing with
    % the new border / TAR approach
    [cmat,labs] = confusionNumeric(data.true.true(:,n),ytest(:,n),...
        data.true.ids{n},train.ids{n},data.include);
    
    % Draw the confusion matrix nicely
    confusionFigure(cmat,labs,n);
    
    % Draw an image showing incorrectly classified hierarchical pixels
    %confusionImage(n,data,ytest(:,n),data.true.ids{n},train.ids{n});
end




assignin('base','ytest',ytest)
assignin('base','data',data);
assignin('base','actL',data.true.ids);
assignin('base','predL',train.ids);


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [files,fList] = doEligibility(fig,files)
% Need to ensure that at least two files are ticked...

data = get(fig.tab, 'Data');
data2 = data';
data3 = cell2mat(data2(1,:));
fx = find(data3 == 1);
fy = data3 == 1;
numF = numel(fx);

% Check for a single file...
if numF < 1
    error('You must select at least 1 file to continue.');
else
    fList = cell(numF,1);
    for n = 1:numF
        files(fx(n)).isTrain = 1;
        files(fx(n)).isTest = 0;
        fList{n,1} = files(fx(n)).name;
    end
end

% Now get rid of the entries in files which weren't selected...
r = 0;
for n = 1:numel(fy)
    
    if fy(n) == 1
        r = r + 1;
        f2(r) = files(n); %#ok<AGROW>
    end
end
files = f2;

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [data] = getData(fig,files)
% Get and format the necessary data parts

h5 = [files(1).path files(1).name];
    
% x
x = h5read(h5,'/X');
sz = size(x);
data.x = reshape(x,[sz(1)*sz(2) sz(3)]);

% groupPixels
anno = h5read(h5,'/groupPixels');
data.anno = zeros(sz(1)*sz(2),size(anno,3));
for n = 1:size(anno,3)
    data.anno(:,n) = reshape(anno(:,:,n),[sz(1)*sz(2) 1]);
end    

% classified pixels (by mmc, can use for 'true' classification
prob = h5read(h5,'/pixelProbs');
data.prob = zeros(size(data.anno));
for n = 1:size(prob,3)
    data.prob(:,n) = reshape(prob(:,:,n),[sz(1)*sz(2) 1]);
end

% class labels, these are text values
i = h5info(h5,'/tissue_id');
numA = size(i.Attributes,1)-1;
data.info = cell(numA,1);
for n = 1:numA
    data.info{n,1} = i.Attributes(n).Value;
end

data.size = sz;

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [cl] = getClassTree(xlp)
% Read / interpret the class tree in the excel file so that it is suitable
% for use in the function

% Read in the file
[~,~,raw] = xlsread(xlp);

% First row is the labels
cl.labs = raw(1,1:end-1);

% Second onwards are the actual classifications
cl.tree = raw(2:end,1:end-1);

% Finally, these are the alternative names for the accepted classification
cl.alt = raw(2:end,end);

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [data] = pixelClass2Hier(data,cl)
% Convert the pixel classifications of the training set to the ytaxa-like
% classifcations featuring all of the levels.

omit = 'none';
thresh = 0.9;

% How many annotated groups?
numG = size(data.info,1);

% How many classification levels?
numC = numel(cl.labs);

% How many branches? i.e. how many rows
numB = size(cl.tree,1);

% Empty place for appropriate classifications
numP = nnz(data.anno);
hier = cell(numP,numC);


% Now for each group, process its pixels and form the training set - this
% is to contain all (except 'omit') groups and is done on a class-by-class
% basis rather than keeping any inherent order to the dataset.
clear hier test train;
for n = 1:numG
    
    % Find the pixels in this tissue class
    [fx,~] = find(data.anno(:,n) == 1);
    
    % Temporary data to form the training set
    tmp = data.x(fx,:);
    
    % Compare data.info{n,1} to the list of alternatives so that we know
    % the class into which to place the pixels.  This allows for
    % alternative spellings of a tissue type, eg. background/Background/bg
    final = '';
    for b = 1:numB
        % Parse the list of alternatives, which must be separated by commas
        % preferably without spaces in the Excel spreadsheet.
        cma = strfind([',' cl.alt{b,:} ','],',');
        s = cl.alt{b,:};
        
        % Run through each alternative
        for c = 1:numel(cma)-1
            t = s(cma(c):cma(c+1)-2);
            if strcmp(data.info{n,1},t)
                final = cl.tree(b,:);
            end
        end        
    end
    
    % So 'final' is the correct tree-like entry for this class and tmp is
    % the data to be used in the training set
    hiertmp = repmat(final,[numel(fx) 1]);
    
    % Append the training set (unless it is the omitted class)
    if strcmp(final(numC),omit)
        % do nothing - this data set is omitted from the training set
        disp(['omit ' omit]);
    else
        if ~exist('hier','var')
            hier = hiertmp;
            train = tmp;
        else
            hier = cat(1,hier,hiertmp);
            train= cat(1,train,tmp);
        end
    end
end
    
% So we now have the training data and their classifcations
data.train = train;
data.hier  = hier;


% TEST SET
% Likely to get very complicated with the adjacent regions. But essentially
% want all pixels from the image to be included whether or not they have a
% good classification already. Use the probs from MMC to put a
% classification on each and every pixel, with the prob' defining the
% ambiguity (i.e. muscle/mucin for dual classification, also need dual
% classification for TO/BG pixels).  Need to avoid having unclassified
% pixels as these have to be deleted as they obviously can't be truly
% classified but can perhaps have a +/Healthy/Unknown classification for
% such pixels and just see where they end up in the end (as there won't be
% this unknown class in the training set).

% Quantity of pixels
numP = size(data.x,1);

% Here is the empty cell ready for the classification of each pixel
data.true = cell(numP,numC);

% A vector to tell the confusion matrix if it is to include the pixel. This
% will be useful for the pixels here that are classified but are far from
% certain in classification terms
data.include = zeros(numP,1);

% Now run through each pixel and find its class (or nearest equivalent)
for n = 1:numP
    
    % Get the probs of this pixel belonging to each class
    pr = data.prob(n,:);
    p5 = pr > 0.5;
    p9 = pr > thresh;
    
    % If sum(p5) > 1 then there are two potentially competing classes for
    % this pixel, so then we need to start looking at how to split it up.    
    if sum(p5) == 1
        % Then only a single competing class. Now need to decide if it is
        % of high enough prob for it to be classified; otherwise, it
        % becomes an unknown.
        if sum(p9) == 1
            % Then definitively classified            
            class = data.info{p9};
            data.true(n,:) = alternativeMatch(cl,numB,class);
            data.include(n,1) = 1;
            
        else % sum(p9) == 0
            % Then this is less certain classification, but at least it is
            % unique. So how to classify it? This is where we classify it
            % but don't let it be included in the confusion matrix
            % statistics
            class = data.info{p5};
            data.true(n,:) = alternativeMatch(cl,numB,class);            
        end
        
    elseif sum(p5) == 2
        % Then two or more competing classes - possiblitities include: both
        % above 0.9; one above; none above 0.9
        [fx] = find(p5 == 1);
        class = sort({data.info{fx(1)}; data.info{fx(2)}});

        % equal split of two classes, so now decide which ones. If one
        % is tumour then proceed down the TAR branch, otherwise stick
        % it in the mix of two healthy regions. This one can be counted
        % in the statistics.
            
        % Decide if there is tumour in this pixel classification
        tar = strcmp(class, 'Tumour');
        if sum(tar) == 1
            fx = find(tar == 0);

            % Must get the initial 'correct' microstate
            tmp = alternativeMatch(cl,numB,class{fx});     

            % Now use the correct microstate to generate the correct
            % tumour-neighbour state...
            data.true(n,:) = alternativeMatch(cl,numB,['T-' tmp{numC}]);

        else
            % This this is a non-tumour border region, so just make up
            % the new classification... Preferably in a repeatable way
            % to ensure that it is always the same. Have previously
            % sorted the states so do it alphabetically...
            tmp = alternativeMatch(cl,numB,'Border');

            % Now replace tmp{numC} with your concatenated classes and
            % that is what you will use
            tmp{numC} = [class{1} '-' class{2}];
            data.true(n,:) = tmp;                                
        end

        % For this case where there are two classes with p > 0.5, only use
        % those where both probs are greater than 0.9.
        if sum(p9) == 2
            % You can include this pixel in the confusion matrix stats
            data.include(n,1) = 1;
        elseif sum(p9) == 1
            % maybe could include if you wanted to?
        else
            % don't include...
        end
        
    else % sum(p5) == 0 || sum(p5) > 2
        % This covers pixels with no classifications AND those with 3 or
        % more classification groups. So just NA it and don't include in
        % the confusion matrix statistics...
        data.true(n,:) = alternativeMatch(cl,numB,'Unknown');
            
    end
    
end

return

% % % Use the prob threshold to assign class membership to the MMC classified
% % % pixels, removing pixels belonging to multiple classes. So pr is the
% % % matrix showing which pixels are to be included as part of the 'true'
% % % classification statistics, i.e. in the confusion matrix. All other
% % % pixels are just there to be classifed to complete the image.
% % pr = data.prob > 0.9;
% % sp = sum(pr,2);
% % [s2,~] = find(sp(:,1) > 1); % dual classified pixels
% % for n = 1:numel(s2)
% %     pr(s2(n),:) = 0;
% % end
% % 
% % 
% % [fx,~] = find(pr(:,n) ~= 0)
% % data.info{n}    
% % for r = 1:numel(fx)
% %     data.true(fx(r),:) = final;
% % end
% % 
% % [fx,~] = find(sum(pr,2) == 0); % delete these rows from data.true
% % data.true(fx,:) = [];
% % 
% % [fx,~] = find(sum(pr,2) > 0); % make data.test using these rows
% % data.test = data.x(fx,:);
% % data.inds = fx;


end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [final] = alternativeMatch(cl,numB,compare)
% Match the alternative names from the xl classification tree returning the
% final hierarchical levels.

for b = 1:numB
    % Parse the list of alternatives, which must be separated by commas
    % preferably without spaces in the Excel spreadsheet.
    cma = strfind([',' cl.alt{b,:} ','],',');
    s = cl.alt{b,:};

    % Run through each alternative
    for c = 1:numel(cma)-1
        t = s(cma(c):cma(c+1)-2);
        if strcmp(compare,t)
            final = cl.tree(b,:);
            return
        end
    end        
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%