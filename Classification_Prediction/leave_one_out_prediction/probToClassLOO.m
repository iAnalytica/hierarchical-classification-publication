function y = probToClassLOO(P,yid)
% Convert  class probabilities into class memberships
% Pthr            = 1/size(P, 2);
[nPixels,~] = size(P);
y           = zeros(nPixels,1);
[sorted_P, ~] = sort(P, 2, 'descend');
Treshold = 0.9;

if(iscell(yid))
    for i = 1:length(y)
        [~, argmax] = max(P(i, :));
        if(ischar(yid{argmax}))
            y(i) = str2double(yid{argmax});
        else
            y(i) = yid{argmax};
        end
        ratio_P2_P1 = (sorted_P(i,1)-sorted_P(i,2))/sorted_P(i,1);
        
        if ratio_P2_P1 < Treshold
%             if str2double(yid{1}) == -1
%                 y(i) = 1;
%             else
                y(i) = NaN;
%             end
        end
    end
else
    for i = 1:length(y)
        [~, argmax] = max(P(i, :));
        if(ischar(yid(argmax)))
            y(i) = str2double(yid(argmax));
        else
            y(i) = yid(argmax);
        end

        ratio_P2_P1 = (sorted_P(i,1)-sorted_P(i,2))/sorted_P(i,1);
        if ratio_P2_P1 < Treshold
%             if str2double(yid(1)) == -1
%                 y(i) = 1;
%             else
                y(i) = NaN;
%             end
        end
    end
end

y(y<0) = NaN;
end