function [  ] = graphHier( cl )
%graphHier - do the biograph hierarchy display for hierarchical
%classification models...

% How many termini are there?
numT = size(cl.tree,1);

% How many branches are there?
numB = size(cl.tree,2);

% Get the names of all of the nodes
id = unique(cl.tree);
id = cat(1,'Pixels',id);

% An empty cm
numN = size(id,1);
cm = zeros(numN,numN);

% So now make the cm for each node. Start from the first branch and work
% towards the most unique branch...  Linkages are added backwards, i.e. so
% if b = 1, then link to 'Pixels', if b = numB link to numB-1
for b = 1:numB
    
    % For each of the nodes...
    for n = 1:numT       
        
        % Match to the global node list. i1 is the index in the global list
        % of this node.
        fx = strcmp(id,cl.tree(n,b));
        i1 = find(fx == 1);
        
        % Now link it to the preceeding branch
        if b == 1
            % Link to root
            cm(1,i1) = 1;
        else
            % Match cl.tree(n,b)
            fy = strcmp(id,cl.tree(n,b-1));
            i2 = find(fy == 1);
            cm(i2,i1) = 1; %#ok<FNDSB>
        end
    end
end

% Create the biograph
bg1 = biograph(cm,id,...
    'LayoutType','hierarchical',...
    'ArrowSize',0);

% Now view it
h1 = view(bg1);




end

