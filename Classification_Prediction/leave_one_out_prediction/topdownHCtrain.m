function tdmodel  = topdownHCtrain(trainX, trainYtaxa, fullYtaxa, speciesList, LOOspStr, LOOspNum, varargin)
%% topdownHCtrain performs training of supervised maching learning algorithms...
%% for hierarchical classification in a top down fashion
%   trainX     - training data set
%   trainYtaxa - training grouping variables (hierarchy)
%   fullYtaxa  - full set of grouping variables (hierarchy)
%% Authors: Kirill A. Veselkov, Imperial College London, 2017
%%          Dieter Galea, Imperial College London, 2017

hBio    = getVarArgin(varargin);
[ytrain,yids,nodes] = getNodeHierarchies(trainYtaxa);

%% nodes.yindcs      - sample indicies associated with a node
%        .id         - node identifier
%        .parentNode - parent node indices
%        .childNode  - child node indices
% yids   - group identifiers
%% ytrain - numerical grouping indicies for taxonomic classification

%% Training phase for taxonomic classification
%%% number of hierarchy levels
nLevels               = size(ytrain,2); 

boolDisplay = true;
if isempty(hBio)
    boolDisplay = false;
end

currNode = 1;
tdmodel.modelid{1}{1} = getmodelid(yids{1}(unique(ytrain(:,1))));
tdmodel.y{1}{1}       = ytrain(:,1);
[~,yid]               = grp2idx(tdmodel.y{1}{1});
tdmodel.yid{1}{1}     = yid;
tdmodel.model{1}{1}   = multivmodelfit(trainX, tdmodel.y{1}{1});

%%%%%%%%%%%%%%%%%%%%%%% for GUI visualization %%%%%%%%%%%%%%%%%%%%%%%
%%% calculate number of nodes
totalNodes = 0;
for iLevel = 1:nLevels-1
    totalNodes = totalNodes + length(unique(ytrain(:,end-1)));
end
clear iLevel

hWaitBar = waitbar(0, 'Classifying...');
movegui(hWaitBar, 'north');
classifiedNodes = 0;

fprintf('Predicting: %s\n',speciesList{LOOspNum});

%%% roll over shows method applied; first set as NA
ShowModel = 'Not Applicable';

%%% start model fitting node-by-node
for iLevel = 1:nLevels-1
    iLevelNodeCount = length(unique(ytrain(:,iLevel)));
    
    for jNodeiLevel  = 1:iLevelNodeCount
        currNode = currNode + 1;
        %%% update the biograph
        if (boolDisplay)
            updateBiograph(hBio, currNode, iLevel, jNodeiLevel, nLevels, ShowModel, iLevelNodeCount, LOOspStr, fullYtaxa);
        end
        classifiedNodes = classifiedNodes+1;
        
        %%% wait bar
        message = sprintf('Classifying Layer %d of %d \n Classifying Node %d of %d', iLevel, nLevels-1, jNodeiLevel, iLevelNodeCount);
        waitbar(classifiedNodes/totalNodes, hWaitBar, message);
        
        y     = nodes.yindcs{jNodeiLevel,iLevel};
        y1idx = unique(y);
        y1idx = y1idx(y1idx~=0);
        
        if sum(y1idx~=0)==1
            parNodeIdx  = [jNodeiLevel,iLevel];
            while sum(unique(y)~=0)==1
                parNodeIdx             = nodes.parentNode{parNodeIdx(1),parNodeIdx(2)};
                if isempty(parNodeIdx)
                    yindcs = ytrain(:,1)~=0;
                else
                    yindcs = nodes.yindcs{parNodeIdx(1),parNodeIdx(2)}~=0;
                end
                y(yindcs~=0) = ytrain(yindcs==1,iLevel+1);
            end
            y2idx            = unique(y(y~=0&y~=y1idx))';
            y(y~=0&y~=y1idx) = -y1idx;
            y(y==y1idx)      = y1idx;
            tdmodel.modelid{iLevel+1}{jNodeiLevel}...
                = getmodelid(yids{iLevel+1}(y1idx),yids{iLevel+1}(y2idx));
        else
            tdmodel.modelid{iLevel+1}{jNodeiLevel}...
                = getmodelid(yids{iLevel+1}(y1idx));
        end
        y(y==0)                               = NaN;
        [y,yid]                               = grp2idx(y);
        tdmodel.y{iLevel +1}{jNodeiLevel}     = y;
        tdmodel.yid{iLevel +1}{jNodeiLevel}   = yid;
        trainXtmp = trainX(~isnan(y),:);
        
        tdmodel.model{iLevel +1}{jNodeiLevel} = multivmodelfit(trainXtmp,...
            y(~isnan(y)));
        ShowModel = tdmodel.model{iLevel+1}{jNodeiLevel}.method;
    end
end
close(hWaitBar);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function modelid = getmodelid(y1ids,y2ids)
%% get model identifier for hierarchical classification:
if nargin == 1
    nGrps = length(y1ids);
    if nGrps==1
        modelid = '';
        return;
    end
    modelid  = y1ids{1};
    for i = 2:nGrps
        modelid = [modelid, ' vs ' y1ids{i}];
    end
else
    nGrps = length(y2ids);
    modelid = [y1ids{1},' vs ', y2ids{1}];
    if nGrps>1
        for i = 2:nGrps
            modelid = [modelid, ', ', y2ids{i}];
        end
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [hBio] = getVarArgin(argsin)
%% get variable input arguments
hBio = [];
nArgs = length(argsin);
for i=1:2:nArgs
    switch argsin{i}
        case 'biograph'
            hBio = argsin{i+1};
    end
end
end