function [yTest,yprobs] = doFlatTrainTest(XTrain,XTest,TrueY,ClassTreeDir,options)
%% Flat classification of test pixels based on train set

% Get ClassTree data
exelPath = [ClassTreeDir 'ClassTree.xlsx'];
[ct]     = getClassTree(exelPath);

% Optional visualisation of tree
% graphHier(ct)

% Get hierachical TrueY
TrueY = TrueY1DtoTrueYHier(ct,TrueY);
TrueY = TrueY(:,end);

% Get options right
[opts,method] = optsToDRoptions(options);

model          = multivmodelfit(XTrain,grp2idx(TrueY),method,opts);
[yTest,yprobs] = multivmodelpredict(XTest,model,num2cell(num2str(unique(grp2idx(TrueY)))));

end