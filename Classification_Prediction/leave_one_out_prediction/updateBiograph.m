function updateBiograph( hBio, currNode, iLevel, jNodeiLevel, nLevels, ShowModel, iLevelNodeCount, predicting_species2, ytaxa)

%%% hBio handle to the biograph
%%% currNode node to be updated
%%% iLevel current level number
%%% jNodeiLevel relative Node for the current Level
%%% iLevelNodeCount is the number of nodes in the current level
%%% ShowModel is the best classification model determined for the current..
%%% ..node to show the roll over label on the biograph

green = [0 1 0];
red = [1 0 0];
defaultColor = [1 1 .7];

%     for sss = 1:(size(ytaxa,2)-1)
%         nh1 = getnodesbyid(hBio, predicting_species2(1,sss));
%         nh1.Color = defaultColor;
%     end

if currNode > 1
    prevNode = currNode - 1;
    desNodes2 = getdescendants(hBio.nodes(prevNode));
    set(desNodes2, 'Color', defaultColor); %default color
    desNodes1 = getdescendants(hBio.nodes(currNode));
    set(desNodes1, 'Color', [1 .7 .7]);
    set(hBio.Nodes(prevNode), 'Color', green);
    LabelX = ShowModel;
    set(hBio.Nodes(prevNode), 'Label', LabelX);
     
    if iLevel == nLevels -1 && jNodeiLevel >=2
        set(desNodes2, 'Color', green); 
        
        if jNodeiLevel == iLevelNodeCount
            set(hBio.Nodes, 'Color', green);
            set(desNodes1, 'Color', green);

        end
    end
end
end

