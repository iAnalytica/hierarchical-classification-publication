function [cmat,labs] = confusionNumeric(act,pred,actL,predL,include)
%confusionNumeric - given numeric values for actual and predicted
%classifications, where the numbers in each correspond to the strings in
%the labs variable.

% Need to modify the approach here, as if a class is omitted from the
% training set then there is a mis-match between the classes when it comes
% to the training set being compared to the test set. So now the function
% uses the labels from each of the test / training sets to compare the
% act/pred cases rather than matching on the numbers, which clearly change
% with different classes.

% Can provide an include vector (1 for yes) of the same length as act/pred.
% The aim is to prevent the inclusion of the training set in the confusion
% matrix as this skews the statistics, especially for groups with few
% pixels. If it is not provided, then all will be included
if nargin == 4
    include = ones(numel(act));
end

% Check that we have the same number in act/pred 
if numel(act) ~= numel(pred)
    error('incorrect classification sizes - cannot continue');
end

% How many groups - this needs to be the 'uniqued' list of both actL/predL
% but is likely to just be those in predL. But account for all
% possibilities
labs = unique([actL;predL]);
numG = numel(labs);

% Empty cmat
cmat = zeros(numG,numG+1);

% Add the entries into the matrix
for n = 1:numel(act)
    
    % First we need to match the actual/true class of the pixel
    ac = actL{act(n)};
    
    % Now get the correct index from labs variable
    fx = strcmp(labs,ac);
    id1 = find(fx == 1);    
    
    if include(n) == 1
        if isnan(pred(n))
            % place into the final column to record NaNs from K's fcn
            cmat(id1,numG+1) = cmat(id1,numG+1) + 1;

        else
            % Now lets get the predicted class of the pixel
            pc = predL{pred(n)};
            fy = strcmp(labs,pc);
            id2 = find(fy == 1);

            % So now place in the matrix...
            cmat(id1,id2) = cmat(id1,id2) + 1;    
        end
    end
end

end

