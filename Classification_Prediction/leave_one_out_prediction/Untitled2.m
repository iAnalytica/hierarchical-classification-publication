%% calculate percentage accuracy for species of interest for iLevel
trainClassesConv = getNodeHierarchies(ytaxa(idxTrain,:));
trainClasses = ytaxa(idxTrain,:);
predStats = tabulate(yPred{LOOspNum}(:,iLevel));
predictedClasses = cell(size(predStats,1),1);
predictionAccuracy = 0;

for iPred = 1:size(predStats,1)
    predictedClasses{iPred} = unique(trainClasses(trainClassesConv(:,iLevel) == predStats(iPred,1),iLevel));
    boolMatch = strcmp(unique(ytaxa(idxTest,iLevel)),predictedClasses{iPred}(:));
    if boolMatch
        predictionAccuracy = predStats(iPred,3);
    end
end

cvaccuracy(iLevel) = predictionAccuracy;%100*sum(yPred{LOOspNum}(:,iLevel) == ytaxaConv(idxTest,iLevel))./length(idxTest);

%             for ss = 1:length(yPred{LOOspNum})
%                 indexVal = yPred{LOOspNum}(ss,iLevel);
%                 if isnan(indexVal)
%                     predSpecies{1,LOOspNum}(ss, iLevel) = cellstr('non-classified');
%                 else
%                     predspeciesIdx 						= find(ytaxaConv(:, iLevel) == indexVal);
%                     predSpecies{1,LOOspNum}(ss,iLevel) 	= ytaxa(predspeciesIdx(1), iLevel);
%                 end
%             end

if iLevel > 1
    ParentNodeID 	= unique(ytaxaConv(idxTest, iLevel-1)); %% gets the parentnode ID
    SisterNodesIdx 	= find(trainYtaxaConv(:, iLevel-1) == ParentNodeID);
    SisterNodesX 	= trainX(SisterNodesIdx,:);
    SisterNodesY 	= trainYtaxaConv(SisterNodesIdx,:);
    SisterNodesYStr = trainYtaxa(SisterNodesIdx,:);
    childIdx 		= ytaxaConv(:,iLevel-1) == ParentNodeID;
    ofInterestIdx 	= idxTestBool.*childIdx;
    childIdx 		= find(ytaxaConv(:,iLevel-1) == ParentNodeID);
    [allIDs,~,b] 	= grp2idx(unique(ytaxaConv(childIdx, iLevel)));
    interestID 		= allIDs(find(b == unique(ytaxaConv(logical(ofInterestIdx),iLevel))));
else
    SisterNodesX 	= trainX;
    SisterNodesY 	= trainYtaxaConv;
    SisterNodesYStr = trainYtaxa;
    ofInterestIdx 	= logical(idxTestBool.*ytaxaConv(:,iLevel));
    [allIDs,~,b] 	= grp2idx(unique(ytaxaConv(:,iLevel)));
    interestID 		= find(b == unique(ytaxaConv(ofInterestIdx)));
end

if isempty(testX) || isempty(SisterNodesX)
end

trainClasses 	= unique(SisterNodesY(:, iLevel), 'stable');
numTrainClasses = length(trainClasses);

if iLevel > 1
    %% Determine good predictions in the previous level
    goodPredictionsIdx = find(yPred{LOOspNum}(:,iLevel-1) == unique(ytaxaConv(idxTest,iLevel-1)));
    %%% How many errors were propagated from previous levels?
    CumError{iLevel} = length(yPred{LOOspNum}(:,iLevel-1)) - length(goodPredictionsIdx);
    newLOOscores{iLevel} = NaN(length(idxTest),2);
    %%% If problem is not binary, take one set of scores
    %%% from the model with the expected class-vs-all
    if isempty(goodPredictionsIdx)
    end
    if iscell(LOOscores{iLevel})
        newLOOscores{iLevel} = LOOscores{iLevel, ParentNodeID}{interestID};
    else
        newLOOscores{iLevel} = LOOscores{iLevel};
    end
else
    CumError{1} = 0;
    newLOOscores{1} = LOOscores{1};
end

hS = figure;
txtClasses = unique(SisterNodesYStr(:, iLevel), 'stable');
idxLegend = 0;
legendTxt = [];
idxNonClass = find(isnan(yPred{LOOspNum}(:,iLevel)));
if iLevel > 1
    prevNonClass = find(isnan(yPred{LOOspNum}(:,iLevel-1)));
    idxNonClass = idxNonClass(~ismember(idxNonClass, prevNonClass));
end
pause(0.1);
%%%%% PLOT NON-CLASSIFIED SAMPLES
if ~isempty(idxNonClass)
    plot(newLOOscores{iLevel}(idxNonClass, 1), newLOOscores{iLevel}(idxNonClass, 2), '.', ...
        'Marker', 'x', ...
        'MarkerSize', 12, ...
        'LineWidth', 2, ...
        'MarkerEdgeColor', 'r');
    idxLegend = idxLegend + 1;
    legendTxt{idxLegend} = ['non-classified'];
end

hold on

%% Exclude red and green plot colors (used as a border color indicate correct/wrong prediction)
ExcludeColors 	= [1 0 0; 0 1 0];

%% Generate 2 distinguishable colors from each other and from red and green
colors 			= distinguishable_colors(numTrainClasses, ExcludeColors);
colors = repmat([163/255 163/255 163/255],numTrainClasses,1);
colorInterest = [107/255, 0, 173/255];
colors(2,:) = colorInterest;

for c = 1:numTrainClasses
    
    idxTestClass = find(yPred{LOOspNum}(:, iLevel) == trainClasses(c));
    idxTrainClass = find(SisterNodesY(:, iLevel) == trainClasses(c));
    
    if iLevel == 1
        ParentNodeID = 1;
    end
    
    if (length(unique(tdmodel.yid{1,iLevel}{ParentNodeID})) <= 2)
        scoresX = tdmodel.model{1,iLevel}{ParentNodeID}.scores(idxTrainClass,1);
        scoresY = tdmodel.model{1,iLevel}{ParentNodeID}.scores(idxTrainClass,2);
    else
        scoresX = tdmodel.model{1,iLevel}{ParentNodeID}.scores{interestID}(idxTrainClass,1);
        scoresY = tdmodel.model{1,iLevel}{ParentNodeID}.scores{interestID}(idxTrainClass,2);
    end
    randomJitter = 2+2.*rand(length(scoresX),1);
    pause(0.1);
    %%% plot training scores (all in same color except class equal to idxTestClass)
    plot(scoresX+randomJitter, scoresY, '.',  ...
        'Marker', 'd', ...
        'MarkerSize', 11, ...
        'MarkerFaceColor', colors(c,:), ...
        'MarkerEdgeColor', 'k');
    pause(0.1);
    if ~isempty(idxTestClass)
        if(unique(yPred{LOOspNum}(idxTestClass, iLevel)) == unique(ytaxaConv(idxTest, iLevel)))
            MarkerEdgeColor = 'g';
        else
            MarkerEdgeColor = 'r';
        end
        
        plot(newLOOscores{iLevel}(idxTestClass,1), newLOOscores{iLevel}(idxTestClass,2), '.', ...
            'Marker', 'o', ...
            'MarkerSize', 11, ...
            'LineWidth', 2, ...
            'MarkerFaceColor', colors(c,:), ...
            'MarkerEdgeColor', MarkerEdgeColor);
    end
    
    if ~isempty(idxTrainClass)
        idxLegend = idxLegend + 1;
        legendTxt{idxLegend} = ['train ', txtClasses{c}];
    end
    
    if ~isempty(idxTestClass)
        idxLegend = idxLegend + 1;
        legendTxt{idxLegend} = ['pred ', txtClasses{c}];
    end
end
hold on
%             plot([0 0],[0 0],'w-');
clear c
box on
hold off

idxLegend = idxLegend +1 ;
legendTxt{idxLegend} = [num2str(CumError{iLevel}),' sample(s) propagated error'];

legendArg = [];
for iLegend = 1:idxLegend
    legendArg = [legendArg, '''', legendTxt{iLegend}, '''', ','];
end
h = eval(['legend(', legendArg(1:end-1), ')']);
set(h, 'FontSize', 13);
oldWinPos = get(hS, 'Position');
set(hS, 'Position', [1 50 oldWinPos(3) oldWinPos(4)]);
predicting_species = ytaxa{idxTest(1), iLevel};
title_var = sprintf('Predicting: %s  \n  Accuracy: %.2f%%', predicting_species, cvaccuracy(iLevel));
title(title_var, 'FontSize', 13);
set(gca, 'FontSize',13);
xlabel(['Discriminant Component 1']);
ylabel(['Discriminant Component 2']);
%pause;
fprintf('Level: %d; %f; \n', iLevel, cvaccuracy(iLevel));
idxLegend = 0;