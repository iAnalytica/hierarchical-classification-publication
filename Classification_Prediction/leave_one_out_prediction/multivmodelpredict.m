function [y,P,visScores] = multivmodelpredict(Xtest,model,yid, currInterestID)
% multivmodelpredict performs supervised feature exctraction via multivariate...
% statistical methods followed by class probability estimation for test set
% Input:
%   Xtest - dataset [number of samples x number of variables]
%   model - trained model
%   yid   - grouping response variable
%
% Output:
%   y     - predicted label
%   P     - posterior probability
%
% Author: Kirill A. Veselkov, Imperial College London 2017
% modified by Paolo Inglese, 2017

%% perform mean centering
Xtest  = getFoldChange(Xtest,model.meanX);

groups   = unique(yid);
nGrps    = length(groups);
nSamples = size(Xtest, 1);
visScores = cell(size(Xtest,1),1);

%% launch prediction
if (nGrps > 2)
    % multiclass - one-vs-all
    P      = zeros(nSamples, nGrps);
    newyid = cell(length(groups), 1);
    
    for g = 1:nGrps
        % SVM needs the bias term and uses only the first dimension for the
        % scores and weights - the second dimension is just for
        % visualisation purposes
        dims = size(model.weights{g},2);
        scores = zeros(nSamples, dims);
        if(strcmp(model.method, 'SVM'))
            preddim = 1;
            for sg = 1:dims
                bias = model.bias{g}(sg);
                scores(:,sg) = Xtest * model.weights{g}(:,sg) + bias;
            end
            if str2double(groups{g}) == currInterestID
                visScores = scores;
            end
        else
            preddim = dims;
            scores = Xtest * model.weights{g}(:,1:dims);
            
            if str2double(groups{g}) == currInterestID
                visScores = scores(:,1:2);
            end 
        end
        Ptmp = mnrval(model.B{g},  scores(:,1:preddim));
        P(:,g) = Ptmp(:,2);
        newyid{g} = yid{g};
    end
    
    y = probToClassLOO(P, newyid);
    
else
    % binary
    % SVM needs the bias term and uses only the first dimension for the
    % scores and weights - the second dimension is just for
    % visualisation purposes
    dims = size(model.weights,2);
    scores = zeros(nSamples, dims);
    if(strcmp(model.method, 'SVM'))
        preddim = 1;
        for sg = 1:dims
            scores(:,sg) = Xtest * model.weights(:,sg) + model.bias(sg);
        end
        visScores = scores;
    else
        preddim = dims;
        scores = Xtest * model.weights(:,1:dims);
        visScores = scores(:,1:2);
    end
    P = mnrval(model.B,  scores(:,1:preddim));
    y = probToClassLOO(P, yid);
end

end