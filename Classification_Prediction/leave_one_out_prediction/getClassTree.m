function [cl] = getClassTree(xlp)
%% Read / interpret the class tree in the excel file so that it is suitable
% for use in the function

% Read in the file
[~,~,raw] = xlsread(xlp);

% Divide into level description and colors
nCols      = size(raw,2); 
midCol     = floor(nCols/2);
LvlCols    = 1:midCol;
ColourCols = midCol+2:nCols;
AltIDs     = ceil(nCols/2);

% First row is the labels
cl.labs = raw(1,1:end);

% Second onwards are the actual classifications
cl.tree = raw(2:end,LvlCols);

% These are the alternative names for the accepted classification
cl.alt = raw(2:end,AltIDs);

% Get the colours
cl.colours = raw(2:end,ColourCols);
end