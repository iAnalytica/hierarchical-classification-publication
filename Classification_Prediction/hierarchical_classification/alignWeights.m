function [tdmodel] = alignWeights(refModel, tdmodel)

if iscell(refModel.weights)
    for g = 1:length(refModel.weights)
        [dirIndcs,dirSigns] = alignCVdirsDR(tdmodel.weights{g},refModel.weights{g});
        tdmodel.weights{g}     = tdmodel.weights{g}(:,dirIndcs);
        tdmodel.scores{g}      = tdmodel.scores{g}(:,dirIndcs);
        tdmodel.loadings{g}    = tdmodel.loadings{g}(:,dirIndcs);
        for i=1:2 %%% number of components (first 2 only)
            tdmodel.weights{g}(:,i) = tdmodel.weights{g}(:,i)*dirSigns(i);
            tdmodel.scores{g}(:,i) = tdmodel.scores{g}(:,i)*dirSigns(i);
            tdmodel.loadings{g}(:,i) = tdmodel.loadings{g}(:,i)*dirSigns(i);
        end
    end
else
    [dirIndcs,dirSigns] = alignCVdirsDR(tdmodel.weights,refModel.weights);
    tdmodel.weights     = tdmodel.weights(:,dirIndcs);
    tdmodel.scores      = tdmodel.scores(:,dirIndcs);
    tdmodel.loadings    = tdmodel.loadings(:,dirIndcs);
    for i=1:2 %%% number of components (first 2 only)
        tdmodel.weights(:,i) = tdmodel.weights(:,i)*dirSigns(i);
        tdmodel.scores(:,i) = tdmodel.scores(:,i)*dirSigns(i);
        tdmodel.loadings(:,i) = tdmodel.loadings(:,i)*dirSigns(i);
    end
end
