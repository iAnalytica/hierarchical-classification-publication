function y = probToClass(P,yid)
% Convert  class probabilities into class memberships
% Pthr            = 1/size(P, 2);
[nPixels,~] = size(P);
y           = zeros(nPixels,1);

if(iscell(yid))
    for i = 1:length(y)
        [~, argmax] = max(P(i, :));
        if(ischar(yid{argmax}))
            y(i) = str2double(yid{argmax});
        else
            y(i) = yid{argmax};
        end
    end
else
    for i = 1:length(y)
        [~, argmax] = max(P(i, :));
        if(ischar(yid(argmax)))
            y(i) = str2double(yid(argmax));
        else
            y(i) = yid(argmax);
        end
    end
end

y(y<0) = NaN;
end