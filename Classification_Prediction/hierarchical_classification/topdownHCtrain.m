function [tdmodel, refModel]  = topdownHCtrain(X, ytaxa, hBio, varargin)
% topdownHCtrain performs training of supervised maching learning algorithms...
% for hierarchical classification in a top-down fashion
%   X      - a training data set
%   ytaxa  - a grouping variable for hierarchical classification
% Authors: Kirill A. Veselkov, Imperial College London, 2017
%          Dieter Galea, Imperial College London, 2017
%          Paolo Inglese, Imperial College London, 2017

[refModel]         = getVarArgin(varargin);
[ytaxa,yids,nodes] = getNodeHierarchies(ytaxa);

%% nodes.yindcs      - sample indices associated with a node
%        .id         - node identifier
%        .parentNode - parent node indices
%        .childNode  - child node indices
% yids   - group identifiers
%% ytrain - numerical grouping indicies for taxonomic classification

%% Training phase for taxonomic classification
nLevels               = size(ytaxa,2); %determine the number of hierarchies

%%% Check if a biograph already exists
boolDisplay     = true;
if isempty(hBio)
    boolDisplay = false;
end

%%% First level is analysed here
tdmodel.modelid{1}{1} = getmodelid(yids{1}(unique(ytaxa(:,1))));
tdmodel.y{1}{1}       = ytaxa(:,1);  
[~,yid]               = grp2idx(tdmodel.y{1}{1});
tdmodel.yid{1}{1}     = yid;

%%% Look for the best method and apply it
[tdmodel.model{1}{1},trainset]   = multivmodelfit(X, tdmodel.y{1}{1});

if ~isempty(refModel)
    tdmodel.model{1}{1} = alignWeights(refModel.model{1}{1},tdmodel.model{1}{1});
end
tdmodel.model{1}{1} = build_after_dr(trainset, tdmodel.model{1}{1});
       
clear yid

%%%%%%%%%%%%%%%%%%%%%%%%%% For GUI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
currNode = 1;
nNodes   = 1; % 1 because of the root node which is not in the ytaxa
classifiedNodes = 0;

hWaitBar = waitbar(0, 'Classifying...'); 

for iLevel = 1:nLevels - 1
    nNodes = nNodes + length(unique(ytaxa(:,iLevel)));
end
clear iLevel

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Now all the other levels are analysed
for iLevel = 1:nLevels-1
    
    % Count all the nodes present in current level
    iLevelNodeCount = length(unique(ytaxa(:,iLevel)));
    
    for jNodeiLevel = 1:iLevelNodeCount
        currNode = currNode + 1;
                    
        %%%%%%%%%%%%%%%% update the biograph (GUI) %%%%%%%%%%%%%%%%%%%%%%%%
        if (boolDisplay)
            updateBiograph(hBio, currNode, iLevel, jNodeiLevel, nLevels, iLevelNodeCount);
        end 

        classifiedNodes = classifiedNodes+1;
        message         = sprintf('Classifying Node %d of %d', currNode, nNodes);
        waitbar(classifiedNodes/nNodes, hWaitBar, message);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        y     = nodes.yindcs{jNodeiLevel,iLevel}; % set non-sister classes as 0s. 
        y1idx = unique(y);
        y1idx = y1idx(y1idx~=0); %%% unique class numbers (e.g 1 and 3)
        
        if sum(y1idx~=0)==1 % in this case we have only one child node
            parNodeIdx  = [jNodeiLevel,iLevel];
            while sum(unique(y)~=0)==1
                parNodeIdx   = nodes.parentNode{parNodeIdx(1),parNodeIdx(2)};
                if isempty(parNodeIdx)
                    yindcs   = ytaxa(:,1)~=0;
                else
                    yindcs   = nodes.yindcs{parNodeIdx(1),parNodeIdx(2)}~=0;
                end
                y(yindcs~=0) = ytaxa(yindcs==1,iLevel+1);
            end
            y2idx            = unique(y(y~=0&y~=y1idx))';
            y(y~=0&y~=y1idx) = -y1idx;
            y(y==y1idx)      = y1idx;
            tdmodel.modelid{iLevel+1}{jNodeiLevel}...
                = getmodelid(yids{iLevel+1}(y1idx),yids{iLevel+1}(y2idx));
        else
            tdmodel.modelid{iLevel+1}{jNodeiLevel}...
                = getmodelid(yids{iLevel+1}(y1idx));
        end
        
        %%% Fit model for each node
        y(y==0)                              = NaN;
        [y,yid]                              = grp2idx(y);
        tdmodel.y{iLevel+1}{jNodeiLevel}     = y;
        tdmodel.yid{iLevel+1}{jNodeiLevel}   = yid;
        [tdmodel.model{iLevel+1}{jNodeiLevel},trainset] = multivmodelfit(X(~isnan(y),:), y(~isnan(y)));
        
        %%% Align weights to the first CV model
        if ~isempty(refModel)
            tdmodel.model{iLevel+1}{jNodeiLevel} = alignWeights(refModel.model{iLevel+1}{jNodeiLevel},tdmodel.model{iLevel+1}{jNodeiLevel});
        end
        tdmodel.model{iLevel+1}{jNodeiLevel} = build_after_dr(trainset, tdmodel.model{iLevel+1}{jNodeiLevel});
        
    end
    clear jNodeiLevel
end
if isempty(refModel)
    refModel = tdmodel;
end
clear iLevel
close(hWaitBar);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function modelid = getmodelid(y1ids,y2ids)
%% get model identifier for hierarchical classification:
if nargin == 1
    nGrps = length(y1ids);
    if nGrps==1
        modelid = '';
        return;
    end
    modelid  = y1ids{1};
    for i = 2:nGrps
        modelid = [modelid, ' vs ' y1ids{i}];
    end
else
    nGrps = length(y2ids);
    modelid = [y1ids{1},' vs ', y2ids{1}];
    if nGrps>1
        for i = 2:nGrps
            modelid = [modelid, ', ', y2ids{i}];
        end
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [refMethod] = getVarArgin(argsin)
%% get variable input arguments
nArgs = length(argsin);
refMethod = [];
for i=1:2:nArgs
    switch argsin{i}
        case 'reference'
            refMethod = argsin{i+1};
    end
end
end