function [y,yids,nodes] = getNodeHierarchies(ytaxa)
%% get inter-connected node structure for hierarchical classification
%  Input: ytaxa - grouping variable for hierarchical classification
%                 [number of samples x number of hierarchies]
%% Author: Kirill A. Veselkov, Imperial College London 2017

%if isnumeric(ytaxa)
%    ytaxa = num2cell(ytaxa);
%end

if size(ytaxa,2)> size(ytaxa,1)
    ytaxa = ytaxa';
end

%% determine the number of class hierarchies
[nSmpls,nHierarchies] = size(ytaxa);
y                     = zeros(nSmpls,nHierarchies);
yids                  = cell(1,nHierarchies);
for i = 1:nHierarchies
    [y(:,i),yids{i}] = grp2idx(ytaxa(:,i));
end

nodes.parentNode = cell(length(yids(nHierarchies)),nHierarchies);

for iHierachy = 1:nHierarchies-1
    yiHierNode  = unique(y(:,iHierachy));
    niHierNodes = length(yiHierNode(yiHierNode~=0));
    for jiHierNode = 1:niHierNodes
        indcs      = y(:,iHierachy)==jiHierNode; ijy = y(:,iHierachy+1); ijy(indcs==0) = 0;
        iHierNodes = unique(y(indcs,iHierachy+1))';
        nodes.yindcs{jiHierNode,iHierachy} = ijy;
        nodes.yid{jiHierNode,iHierachy}    = yids{iHierachy+1}(iHierNodes);
        if iHierachy == 1
            nodes.id{jiHierNode,iHierachy} = num2str(jiHierNode);
        end
        indx = 1;
        for kChildNode = iHierNodes
            nodes.parentNode{kChildNode,iHierachy+1}    = [jiHierNode,iHierachy];
            nodes.childNode{jiHierNode,iHierachy}{indx} = [kChildNode,iHierachy+1] ;
            nodeidinx = nodes.parentNode{jiHierNode,iHierachy+1};
            nodes.id{kChildNode,iHierachy+1}  = [nodes.id{nodeidinx(1),nodeidinx(2)},...
                '_', num2str(kChildNode)];
            indx = indx + 1;
        end
    end
end

return;