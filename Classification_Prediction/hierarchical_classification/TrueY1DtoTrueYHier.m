function TrueYHier = TrueY1DtoTrueYHier(ct,TrueY)
%% Get hierachical 2D TrueY array from 1D TrueY vector for topdownHCtrain
%

% Prepare variables
nLevels   = size(ct.tree,2);
hIDs      = ct.tree(:,nLevels);
nHistIDs  = length(hIDs);
nPixels   = size(TrueY,1);
TrueYHier = cell(nPixels,nLevels);

% Fill hierachical TrueY (histIDs) with corresponding level identities
for i = 1:nHistIDs
%     idxhID = strcmp(TrueY,hIDs(i));
%     if sum(idxhID)==0
        [idxhID] = alternativeMatch(ct,i,TrueY);
%     end
    for j = 1:nLevels
        TrueYHier(idxhID,j) = ct.tree(i,j);
    end
end

end

function [indices] = alternativeMatch(ct,i,TrueY)
%% Match the alternative names from the xl classification tree returning 
%  the actual indices.

% Parse the list of alternatives, which must be separated by commas
% preferably without spaces in the Excel spreadsheet.
cma = strfind([',' ct.alt{i,:} ','],',');
s = ct.alt{i,:};

% Run through each alternative
for c = 1:numel(cma)-1
    indices = strcmp(TrueY,{s(cma(c):cma(c+1)-2)});
    if sum(indices)>0
        return
    end
end

end