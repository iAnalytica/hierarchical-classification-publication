function model = build_after_dr(trainset, model)
% build a logistic regression model after dimensionality reduction for
% probability estimation
%
% Author: Paolo Inglese, Imperial College, 2017

groups   = unique(trainset.groupdata);
nGrps    = length(groups);
nSamples = length(trainset.groupdata);

if nGrps > 2
    % multiclass - one-vs-all
    for g = 1:nGrps
        dims = size(model.scores{g},2);
        if (strcmp(model.method,'SVM'))
            dims = 1;
        end
        newy = ones(nSamples, 1);
        newy(trainset.groupdata == groups(g)) = 2;
        model.B{g} = mnrfit(model.scores{g}(:,1:dims), newy);
    end
else
    % binary
    dims = size(model.scores, 2);
    if(strcmp(model.method, 'SVM'))
        dims = 1;
    end
    model.B = mnrfit(model.scores(:, 1:dims), trainset.groupdata);
end

end