function [y,P,scoresX,scoresY] = multivmodelpredict(Xtest,model,yid)
% multivmodelpredict performs supervised feature extraction via multivariate...
% statistical methods followed by class probability estimation for test set
% Input:
%   Xtest - dataset [number of samples x number of variables]
%   model - trained model
%   yid   - grouping response variable
%
% Output:
%   y     - predicted label
%   P     - posterior probability
%
%% Author: Kirill A. Veselkov, Imperial College London 2017
%% Modified by: Paolo Inglese, 2015 & Dieter Galea, 2017

groups   = unique(yid);
nGrps    = length(groups);
nSamples = size(Xtest, 1);

%% launch prediction
if (nGrps > 2)
    % multiclass - one-vs-all
    P      = zeros(nSamples, nGrps);
    newyid = cell(length(groups), 1);
    scoresX      = zeros(nSamples, nGrps);
    scoresY      = zeros(nSamples, nGrps);
    X = Xtest;
    for g = 1:nGrps
        % SVM needs the bias term and uses only the first dimension for the
        % scores and weights - the second dimension is just for
        % visualisation purposes
        dims = size(model.weights{g}, 2);
        scores = zeros(nSamples,dims);
        Xtest = X;
        if(strcmp(model.method, 'SVM'))
            preddim = 1;
            for sg = 1:dims
                bias = model.bias{g}(sg);
                scores(:,sg) = Xtest * model.weights{g}(:,sg)+bias;
%                 Xtest = Xtest - (Xtest*model.weights{g}(:,sg))*model.weights{g}(:,sg)';
            end
            clear sg
        else
            preddim = dims;
            scores = Xtest * model.weights{g}(:,1:dims);
        end
        scoresX(:,g) = scores(:,1);
        scoresY(:,g) = scores(:,2);
            
        Ptmp = mnrval(model.B{g}, scores(:,1:preddim));
        P(:,g) = Ptmp(:,2);
        newyid{g} = yid{g}; 
    end
    clear g nGrps dims preddim bias scores Ptmp
    
    y = probToClass(P, newyid);
    
else
    % binary
    % SVM needs the bias term and uses only the first dimension for the
    % scores and weights - the second dimension is just for
    % visualisation purposes
    dims = size(model.weights, 2);
    scores = zeros(nSamples, dims);
    if(strcmp(model.method, 'SVM'))
        preddim = 1;
        for sg = 1:dims
            scores(:,sg) = Xtest*model.weights(:,sg)+model.bias(sg);
%             Xtest = Xtest - (Xtest*model.weights(:,sg))*model.weights(:,sg)';
        end
        clear sg
    else
        preddim = dims;
        scores = Xtest * model.weights(:,1:dims);
    end
    scoresX(:,1) = scores(:,1);
    scoresY(:,1) = scores(:,2);
    
    P = mnrval(model.B,  scores(:,1:preddim));
    y = probToClass(P, yid);
    
end
end