function drawBiograph(ytaxa,tdmodel, testData)
%%% draw biograph
[adjM,yids,~] = getHCAdjMatrix([repmat({'root'}, size(ytaxa, 1), 1), ytaxa]);
bg1 = biograph(adjM,yids,'LayoutType','hierarchical','ArrowSize',0);%,'ytaxa',ytaxa);
hBio = view(bg1);
nNodes = length(yids);
set(hBio.Nodes, 'UserData', struct('ytaxa',{ytaxa},'models',tdmodel,'testData',{testData}));
set(hBio, 'NodeCallBack', {@NodeClick});
end

function NodeClick(hobject)
%%% node callback
currNode = hobject;
UserData = get(currNode, 'UserData');
ytaxa    = UserData.ytaxa;
ytaxaConv = getNodeHierarchies(ytaxa);

%%% find which node was clicked
if strcmp(currNode.ID,'root')
    ClickedNodeID = 'root';
    iLevel = 0;
else
    [nodeIdx,iLevel] = find(strcmpi(currNode.ID, ytaxa));
    iLevel = unique(iLevel);
    ClickedNodeID = unique(ytaxaConv(nodeIdx, iLevel));
end

drawPlots(ytaxaConv, ytaxa, iLevel, ClickedNodeID, hobject);
end


function drawPlots(ytaxaConv, ytaxa, iLevel, jNode, hobject)

%%% retrieve data from figure
currNode = hobject;
UserData = get(currNode, 'UserData');


if strcmp(jNode, 'root')
    Children = unique(ytaxaConv(:,iLevel+1));
    jNode = 1;
else
    ClickedNodeIdx = ytaxaConv(:,iLevel) == jNode; %%% ClickedNode = Parent Node
    Children = unique(ytaxaConv(ClickedNodeIdx,iLevel+1));
end
nClasses = length(Children);

%%% create a plot for each class (for one-vs-all) or one plot for binary
if nClasses == 2
    figure;
    plotSubFigure(UserData, nClasses, 1, ytaxaConv, ytaxa, iLevel, jNode, Children);
elseif nClasses > 2
    figure;
     for iClass = 1:nClasses
         plotSubFigure(UserData, nClasses, iClass, ytaxaConv, ytaxa, iLevel, jNode, Children);
     end
else
    %%% do nothing - only one child node present
end
end


function plotSubFigure(UserData, nClasses, iClass, ytaxaConv, ytaxa, iLevel, jNode, Children)

tdmodel = UserData.models;
testIdx = UserData.testData.testIdx;
predClasses = UserData.testData.ycv;
testScoresX = UserData.testData.scoresX;
testScoresY = UserData.testData.scoresY;

nCVs = size(testIdx,2);
uniqueTheor = unique(ytaxaConv(ismember(ytaxaConv(:,iLevel+1), Children),iLevel+1));

    for iCV = 1:nCVs
        %%% get predictions and theoretical classes
        iCVidx = testIdx(:,iCV);
        iCVpredtmp = predClasses(iCVidx,iLevel+1);
        iCVtheortmp = ytaxaConv(iCVidx, iLevel+1);
        
        if nClasses > 2
            nRows = ceil(sqrt(nClasses));
            nCols = ceil(nClasses / nRows);
            subplot(nRows, nCols, iClass);
        end
        
        %%% get only clicked node children
        childrenIdx = ismember(iCVtheortmp, Children);

        iCVtheor = iCVtheortmp(childrenIdx);
        iCVpred = iCVpredtmp(childrenIdx);
        
        %%% generate distinguishable colors based on nClasses
        uniqueClassColors = distinguishable_colors(nClasses+1);

        %%% generate border colors - predicted class
        borderColors = zeros(length(iCVpred), 3);
        iClassPredIdx = iCVpred == uniqueTheor(iClass);
        restIdx = iCVpred ~= uniqueTheor(iClass);
        borderColors(iClassPredIdx,:) = repmat(uniqueClassColors(iClass,:), sum(iClassPredIdx),1);
        borderColors(restIdx,:) = repmat(uniqueClassColors(end,:), sum(restIdx),1);
        
        % border legend
        borderNames = cell(length(iClassPredIdx),1);
        iClassName = unique(ytaxa(ytaxaConv(:,iLevel+1) == uniqueTheor(iClass),iLevel+1));
        
        borderNames(iClassPredIdx) = iClassName;
        if nClasses == 2
            otherClassName = unique(ytaxa(ytaxaConv(:,iLevel+1) == uniqueTheor(iClass+1),iLevel+1));
            borderNames(restIdx) = otherClassName;
        else
            borderNames(restIdx) = {'rest'};
        end
        [uniqueBorderColors,uniqueVals,uniqueBorderIdx] = unique(borderColors,'rows','stable');
        borderNames = borderNames(uniqueVals);
        
        %%% generate face colors - actual class
        faceColors = zeros(length(iCVpred),3);
        iClassTheorIdx = iCVtheor == uniqueTheor(iClass);
        restIdx = iCVtheor ~= uniqueTheor(iClass);
        faceColors(iClassTheorIdx,:) = repmat(uniqueClassColors(iClass,:), sum(iClassTheorIdx),1);
        faceColors(restIdx,:) = repmat(uniqueClassColors(end,:), sum(restIdx),1);
        
        %%% face legend
        faceNames = unique(ytaxa(ismember(ytaxaConv(:,iLevel+1), uniqueTheor),iLevel+1),'stable');

        if nClasses == 2            
            iClasshBorder = scatter(testScoresX{iLevel+1,jNode,iCV}(childrenIdx,1),...
                testScoresY{iLevel+1,jNode,iCV}(childrenIdx,1),...
                100, borderColors); 
            hold on
            iClasshFace = scatter(testScoresX{iLevel+1,jNode,iCV}(childrenIdx,1),...
                testScoresY{iLevel+1,jNode,iCV}(childrenIdx,1),...
                40, faceColors,'filled');
            hold on
            
            %%%% don't show default legend
            hasbehavior(iClasshBorder, 'legend',false);
            hasbehavior(iClasshFace, 'legend', false);

        else
            otherClasshBorder = scatter(testScoresX{iLevel+1,jNode,iCV}(childrenIdx,iClass),...
                testScoresY{iLevel+1,jNode,iCV}(childrenIdx,iClass),...
                100, borderColors);
            hold on
            otherClasshFace = scatter(testScoresX{iLevel+1,jNode,iCV}(childrenIdx,iClass),...
                testScoresY{iLevel+1,jNode,iCV}(childrenIdx,iClass),...
                40, faceColors, 'filled');
            hold on
            
            %%% don't show default legend
            hasbehavior(otherClasshBorder, 'legend',false);
            hasbehavior(otherClasshFace, 'legend',false);

        end
    hold on
    end
    
    for iFace = 1:length(faceNames)
        faceNames{iFace} = [faceNames{iFace}, ' (actual)'];
    end
    
    for iBorder = 1:length(borderNames)
        borderNames{iBorder} = [borderNames{iBorder}, ' (predicted)'];
    end
    %%% show legend
    if nClasses == 2
        %%% faces
        scatter(NaN, NaN, 40, uniqueClassColors(1,:), 'filled');
        scatter(NaN, NaN, 40, uniqueClassColors(end,:), 'filled');
        %%% borders
        scatter(NaN, NaN, 100,uniqueBorderColors(1,:));
        scatter(NaN, NaN, 100,uniqueBorderColors(end,:));
        %%% borderNames
        legend([faceNames; borderNames]);
        hold on
    else
        %%% faces
        scatter(NaN, NaN, 40, uniqueClassColors(iClass,:), 'filled');
        scatter(NaN, NaN, 40, uniqueClassColors(end,:), 'filled');
        %%% borders
        scatter(NaN, NaN, 100,uniqueBorderColors(1,:));
        scatter(NaN, NaN, 100,uniqueBorderColors(end,:));
        tmplgd = [faceNames(iClass); {'rest (actual)'}; borderNames];
        legend(tmplgd);
        hold on 
    end
    xlabel('Discriminant Component #1');
    ylabel('Discriminant Component #2');
end