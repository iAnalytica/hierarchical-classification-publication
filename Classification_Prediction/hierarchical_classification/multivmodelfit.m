function [bestModel,trainset] = multivmodelfit(X,y)
% multivmodelfit performs supervised feature exctraction via multivariate...
% statistical methods followed by class probability estimation
%   X - dataset [number of samples x number of variables]
%   y - grouping response variable
% Author: Kirill A. Veselkov, Imperial College London 2017
% modified by Dieter Galea, Paolo Inglese, 2017

%% CV on training set to determine best classifier; by default use K = 5
Kfold = 5;

% check if it's enough to be sure all classes have at least one sample in
% the training set and one in the test set.
classLabelArray    = unique(y);
nClassLabelArray   = length(classLabelArray);
numObs             = size(X, 1);
tmpNfold           = zeros(nClassLabelArray,1);

for iC = 1:nClassLabelArray
    numSamplesPerClass = length(y == classLabelArray(iC));
    if(numSamplesPerClass < round(numObs/Kfold)+1)
        tmpNfold(iC) = round(numObs/(numSamplesPerClass-1));
    end
end
%%% if there is not at least one sample in training set, readjust Kfold
if(nnz(tmpNfold)>0)
    fprintf(2,'The number of folds is too small for the number of samples per class\nn is set to %d', max(tmpNfold));
    Kfold = max(tmpNfold);
end

clear tmpNfold numSamplesPerClass numObs classLabelArray

%% start the cross-validation
% do the partition
c                  = cvpartition(numel(y), 'k', Kfold);
methods            = {'PLS','LDAMMC','SVM','PCALDA'};
methods_accuracies = zeros(Kfold,length(methods));
comput_time        = zeros(Kfold, length(methods));

for iCV = 1:Kfold
    
    trainIdx = training(c, iCV);
    testIdx = test(c, iCV);
    
    %%% mean center training and test set
    m          = mean(X(trainIdx,:),1);
    Xtrain     = getFoldChange(X(trainIdx,:),m); 
    Xtest      = X(testIdx, :);
    Xtest      = getFoldChange(Xtest,m); 
    yTrain     = y(trainIdx);
    yTest      = y(testIdx);
    
    trainset.groupdata  = yTrain;
    trainset.X          = Xtrain;
    
    fprintf('trying: ');
    
    % try all the methods
    for iClassifier = 1:length(methods)
        
        fprintf('%s\n', methods{iClassifier});
        
        % select the method from those available
        trainset.method = methods{iClassifier};
       
        % start measuring running time
        tic;
        
        % dimensionality reduction
        mdl   = doDR(trainset); % mdl.loadings, mdl.scores, mdl.weights  - output
        mdl.X = [];             % remove training datasets
        
        % after the dimensionality reduction, a logistic regression model
        % is fitted on the scores for the posterior probability estimation
        [y_predicted] = build_and_predict_after_dr(trainset, Xtest, mdl);
        
        % validate
        methods_accuracies(iCV, iClassifier) = nnz(yTest == y_predicted) / numel(yTest);
        
        % compute learning and testing time
        comput_time(iCV, iClassifier) = toc;
    end
end
clear mdl iCV iClassifier Xtest Xtrain yTrain yTest Kfold

%% Determining best classifier/method (based on mean highest accuracies, and if equal, best mean comput time)
avg_comput_time         = mean(comput_time,1);
avg_acc                 = mean(methods_accuracies,1);
summaryResults          = vertcat(avg_acc, avg_comput_time);
maxAccuracy             = max(summaryResults(1,:));
maxIndex                = find(summaryResults(1, :) == maxAccuracy);
clear avg_comput_time avg_acc 

if length(maxIndex) > 1
    bestAcc         = summaryResults(:, maxIndex);
    minTime         = min(bestAcc(2, :));
    bestMethodidx   = find(summaryResults(1, :) == maxAccuracy & summaryResults(2, :) == minTime);
    bestModel       = methods{bestMethodidx};  %#ok<*FNDSB>
else
    bestMethodidx   = maxIndex;
    bestModel       = methods{bestMethodidx};
end
clear bestAcc minTime bestMethodidx maxIndex

%% train using the best method
trainset.groupdata  = y;
m                   = mean(X,1);
X                   = getFoldChange(X,m);
trainset.meanX      = m;
trainset.X          = X;
trainset.method     = bestModel;

fprintf(2, 'Applying %s\n', bestModel);
fprintf(2, 'acc. on train set: %.2f%%\n', maxAccuracy*100);

% dimensionality reduction
bestModel       = doDR(trainset); % model.loadings, model.scores, model.weights  - output
bestModel.X     = []; % remove training datasets
bestModel.meanX = m;

end

