function HierClassification(X,ytaxa,varargin)
%%% Input:  X       - expression matrix (m x n) where m = samples and n = features
%%%         ytaxa   - class assignment where each sample should have a corresponding
%%%                   row label. Columns indicate the tree levels. A 200 x 5 matrix
%%%                   means 200 samples with 5 hierarchy levels.
%% Authors: Kirill A. Veselkov, Imperial College London, 2017
%%          Dieter Galea, Imperial College London, 2017
%%          Paolo Inglese, Imperial College London, 2017

%% remove the extra 'root' column, if it exists
if strcmpi(ytaxa(1), 'root');
    ytaxa = ytaxa(:, 2:end);
end
ytaxaOrg = ytaxa;
%% draw classification tree as a biograph
[adjM,yids,~]   = getHCAdjMatrix([repmat({'root'}, size(ytaxa, 1), 1), ytaxa]);
bg1             = biograph(adjM,yids,'LayoutType','hierarchical','ArrowSize',0);
clear adjM yids

%% start detection of optimal classification tree
[ytaxa,yids]    = getNodeHierarchies(ytaxa);

%% setup global variables
nSamples        = size(X,1);
cvAccuracies    = NaN(size(ytaxa));
nLevels         = length(yids);
stackedConfMat  = cell(1,nLevels); %% holds confusion matrices for all levels
prompt          = {'Cross validation rounds:', 'Number of Prediction Repetitions'};
dlg_title       = 'CV setup';
num_lines       = 1;
defaultans      = {'5','1'};
UserAns         = str2double(inputdlg(prompt,dlg_title,num_lines,defaultans));
nReps           = UserAns(2); %% number of repetitions to perform classification and prediction
nCVs            = UserAns(1); %% number of cross-validations used for model building
[cvindcs]       = getVarArgin(varargin,nSamples, nCVs);

%% clear variables not used anymore
clear prompt dlg_title num_lines defaultans UserAns

%% main loop repeating classification for nReps
for iRep = 1:nReps
    fprintf('Repetition: %d\n', iRep);
    
    % cross-validation rounds
    for jRound = 1:nCVs
        fprintf(2, 'CV: %d\n', jRound);
        
        % first close the previous biograph, if it exists
        clean_figures();
        
        % plot a biograph with the taxonomic tree
        pause(0.1);
        hBio    = view(bg1);

        % define training and test sets
        test    = (cvindcs == jRound);
        train   = ~test;

        %% train models
        Xtrain  = X(train,:);
        Xtest   = X(test,:);
        
        % use CV round 1 as reference model for weight alignment for 
        % subsequent CV rounds
        if jRound == 1
            [tdmodel, refModel]     = topdownHCtrain(Xtrain, ytaxa(train,:), hBio);
        else
            [tdmodel, ~]            = topdownHCtrain(Xtrain,ytaxa(train,:),hBio, 'reference', refModel);
        end
        
        %% predict test from the train models
        [cvAccuracies(test,:), ~, testScoresX, testScoresY] = topdownHCpredict(Xtest,tdmodel);
        
        clear Xtest Xtrain
        
        % if its the first cross-validation (CV), create a n-dimensional matrix where 
        % n= number of CVS, to hold the X and Y scores for each CV. Also
        % save the indices used for that repetition.
        if jRound == 1
            StackedTestScoresX  = cell(size(testScoresX,1), size(testScoresX,2), nCVs);
            StackedTestScoresY  = cell(size(testScoresY,1), size(testScoresY,2), nCVs);
            trainIdx        = train;
            testIdx         = test;
        else 
            trainIdx        = cat(2,trainIdx,train);
            testIdx         = cat(2,testIdx,test);
        end
        
        % save the test scores for this CV round
        StackedTestScoresX(:,:,jRound) = testScoresX;
        StackedTestScoresY(:,:,jRound) = testScoresY;
        
        % clear current CV scores
        clear testScoresX testScoresY
        
        % add the selected method (i.e. PLS, SVM, etc.) in the current CV 
        % to a summary table 
        for iModel = 1:length(tdmodel.model)
            for jModel = 1:length(tdmodel.model{iModel})
                selmethod{iModel,jModel}.method{jRound}  = tdmodel.model{iModel}{jModel}.method;
                selmethod{iModel,jModel}.options{jRound} = tdmodel.model{iModel}{jModel}.options;
            end
        end
    end %% end of CV rounds
    
    %% generate a biograph with respective score plots for callbacks %%
    testData.testIdx    = testIdx;
    testData.trainIdx	= trainIdx;
    testData.scoresX    = StackedTestScoresX;
    testData.scoresY    = StackedTestScoresY;
    testData.ycv        = cvAccuracies;
    drawBiograph(ytaxaOrg,tdmodel, testData);
    
    %% accumulate confusion matrices for each level for each repetition %%
    for iLevel = 1:nLevels
        
        % check if there are any non-classified classes/species in this level
        nanindcs = isnan(cvAccuracies(:,iLevel));
        
        if sum(nanindcs)>0; % there are non-classified classes!
            grpidx                        = max(cvAccuracies(~nanindcs,iLevel))+1;
            cvAccuracies(nanindcs,iLevel) = grpidx;
            yids{iLevel}{grpidx}          = 'non-classified';
            nonclass                      = 1; %%% there are non-classified samples
        else
            nonclass                      = 0; %%% there are no non-classified samples
        end
        
        % accumulate conf matrices for nReps into cumulativeConfMat
        if iLevel == 1 && iRep == 1
            [cumulativeClassAcc, confMat, ~, stdv] = getConfMatDR(ytaxa(:,iLevel),cvAccuracies(:,iLevel),nonclass,iRep, iLevel);
        else
            [cumulativeClassAcc, confMat, ~, stdv] = getConfMatDR(ytaxa(:,iLevel),cvAccuracies(:,iLevel),nonclass,iRep, iLevel, 'cumMat', cumulativeClassAcc);
        end
        % calculate standard deviation for each level
%         stdev(iLevel) = stdv;
        
        % if there are no non-classifieds, add a row and column with zeroes for non-classifieds
        % if not this will be present - just for consistency 
        if nonclass == 0
            rowZeros    = zeros(1,(size(confMat.acc,1)+1));
            columnZeros = zeros(size(confMat.acc,2),1);
            confMat.acc = horzcat(confMat.acc, columnZeros);
            confMat.acc = vertcat(confMat.acc, rowZeros);
        end
        
        % stack the confusion matrices
        stackedConfMat{iLevel} = cat(3,stackedConfMat{iLevel}, confMat.acc);
        
        % visualize the species level accuracy
        if(iLevel==6)
            fprintf(2, 'Species accuracy: %.2f%%\n', mean(diag(confMat.acc(1:end-1,1:end-1))));
        end
    end %end iLevel loop
    clear iLevel
end
clear iRep
clean_figures()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% generate results %%%%%%%%%%%%%%%%%%%%%%%%%%%%
for iLevel = 1:nLevels
    nStrings = length(yids{iLevel});
    if strcmp(yids{iLevel}{nStrings},'non-classified')
    else
        yids{iLevel}{nStrings+1} = 'non-classified';
    end
    
    %%calculate mean, maximum and minimum for all level for all the nReps
    %%(based on 90% of data - to exclude 'outliers')
    stackedConfMat{iLevel} = sort(stackedConfMat{iLevel},3);
    if(nReps > 1)
        removeEntries = round(0.05*nReps);
        stackedConfMat{iLevel} = stackedConfMat{iLevel}(:,:, (removeEntries+1:nReps-removeEntries-1));
    end
    sortedMin   = min(stackedConfMat{iLevel}, [], 3);
    sortedMax   = max(stackedConfMat{iLevel}, [], 3);
    sortedMean  = mean(stackedConfMat{iLevel}, 3);
    sortedStd   = std(stackedConfMat{iLevel},[],3);
    
    summaryConfMat{iLevel}.acc = sortedMean;
    summaryConfMat{iLevel}.min = sortedMin;
    summaryConfMat{iLevel}.max = sortedMax;
    summaryConfMat{iLevel}.std = sortedStd;
    
    %%remove the non-classifieds for overall level accuracy and stdev calculation
    reducedConfMat{iLevel}.mean = sortedMean(1:end-1, 1:end-1);
    reducedConfMat{iLevel}.min  = sortedMin(1:end-1,1:end-1);
    reducedConfMat{iLevel}.max  = sortedMax(1:end-1,1:end-1);
    reducedConfMat{iLevel}.std  = sortedStd(1:end-1,1:end-1);
    
    LevelMean(iLevel) = mean(diag(reducedConfMat{iLevel}.mean));
    LevelMax(iLevel)  = mean(diag(reducedConfMat{iLevel}.max));
    LevelMin(iLevel)  = mean(diag(reducedConfMat{iLevel}.min));
    LevelStd(iLevel)  = mean(diag(reducedConfMat{iLevel}.std));
    
    %%% plot confusion matrix
    pause(0.1); drawnow;
    plotConfMatDR(summaryConfMat{iLevel},yids{iLevel}, ytaxa, iLevel, 10);
    titleStr = title(sprintf('Average Level Accuracy: %.1f%% (%.1f - %.1f%%)', LevelMean(iLevel), LevelMax(iLevel), LevelMin(iLevel)));
    str = ['Level', num2str(iLevel)];
    %%% save confusion matrices
    print(gcf,str,'-djpeg', '-r300');
end

%%% save accuracies in txt files
RepAvg  = ['HierAcc_', num2str(nCVs), 'CV_', num2str(nReps), 'RepAvg.txt'];
faccid1 = fopen(RepAvg,'w');
RepSTD  = ['HierSTD_', num2str(nCVs), 'CV_', num2str(nReps), 'RepStd.txt'];
faccid2 = fopen(RepSTD,'w');
RepMax  = ['HierAcc_', num2str(nCVs), 'CV_', num2str(nReps), 'RepMax.txt'];
faccid3 = fopen(RepMax,'w');
RepMin  = ['HierAcc_', num2str(nCVs), 'CV_', num2str(nReps), 'RepMin.txt'];
faccid4 = fopen(RepMin,'w');

fprintf(faccid1,'%.2f,',LevelMean);
fprintf(faccid2, '%.2f,', LevelStd);
fprintf(faccid3, '%.2f,', LevelMax);
fprintf(faccid4, '%.2f,', LevelMin);

fprintf(faccid1, '\n');
fprintf(faccid2, '\n');
fprintf(faccid3, '\n');
fprintf(faccid4, '\n');

fclose(faccid1); fclose(faccid2); fclose(faccid3); fclose(faccid4);

% %%% calculate average accuracies and stdeviations for each specific class
% iLevel = 0;
%
% for iLevel = 1:nLevels
%     for nClasses = 1:length(cumulativeConfMat{1,iLevel})
%         ClassesAcc = [];
%         for rep = 1:nReps
%             ClassesAcc = [ClassesAcc, cumulativeConfMat{rep,iLevel}(nClasses,1)];
%         end
%         clear reps
%         clear ClassesAcc
%     end
%     clear nClasses
% end
end

function [cvindcs] = getVarArgin(argsin, N, nCV)
%% get variable input arguments
cvtype  = {'Kfold',nCV};
nArgs   = length(argsin);
for i=1:2:nArgs
    switch argsin{i}
        case 'cvtype'
            cvtype = argsin{i+1};
    end
end

switch cvtype{1}
    case 'Kfold'
        cvindcs = crossvalind('Kfold', 1:N, cvtype{2});
    case 'LeaveMOut'
        cvindcs = crossvalind('LeaveMOut', 1:N, cvtype{2});
end
end

function clean_figures()
close;
child_handles = allchild(0);
names = get(child_handles,'Name');
nBiograph = strncmp('Biograph Viewer', names, 15);
close(child_handles(nBiograph));      
end