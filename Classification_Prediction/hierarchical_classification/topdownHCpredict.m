function [ytest,P,scoresX, scoresY] = topdownHCpredict(Xtest, tdmodel)
%% topdownHCpredict performs training of supervised maching learning algorithms...
%% for hierarchical top-down classification
%   Xtest   - a test set
%   tdmodel - fitted model for hierarchical classification
%% Author: Kirill A. Veselkov, Imperial College London, 2017
%%         Dieter Galea, Imperial College London, 2017

%% Prediction phase
nLevels  = length(tdmodel.y); %determine the number of class hierarchies
nObs     = size(Xtest,1);
ytest    = NaN(nObs,nLevels);
P        = cell(1,nLevels);
scoresX  = cell(1,nLevels);
scoresY  = cell(1,nLevels);

for iLevel = 1:nLevels
    nModelsiLevel = length(tdmodel.model{iLevel});
    for jModel = 1:nModelsiLevel
        nClasses         = length(tdmodel.yid{iLevel}{jModel});
        P{iLevel,jModel} = NaN(nObs,nClasses);
        if nClasses > 2
            nCls = nClasses;
        else
            nCls = 1;
        end
        scoresX{iLevel,jModel} = NaN(nObs,nCls);
        scoresY{iLevel,jModel} = NaN(nObs,nCls);
    end
    clear jModel nModelsiLevel nClasses nCls
end

obsindcs  = ones(nObs,1)==1;

clear iLevel nLevels nObs

[ytest,P,scoresX,scoresY] = topdownHCrecpred(Xtest, tdmodel, ytest, P, 1, 1,obsindcs,scoresX, scoresY);
end

function  [ytest,P,scoresX,scoresY] = topdownHCrecpred(Xtest, tdmodel, ytest, P, iLevel, jNode, obsindcs, scoresX,scoresY)
%% using recursive algorithm for topdown classification

%% convergence criterium for recursion
if sum(obsindcs) ==0
    return;
end

%% fit model at a givel level of hierarchy
%%% mean center
m     = tdmodel.model{iLevel}{jNode}.meanX;
testX = getFoldChange(Xtest(obsindcs,:),m);

if ~isempty(tdmodel.model{iLevel}{jNode})
    [ytest(obsindcs,iLevel),P{iLevel,jNode}(obsindcs,:), ...
        scoresX{iLevel,jNode}(obsindcs,:), scoresY{iLevel,jNode}(obsindcs,:)] = multivmodelpredict(testX,...
        tdmodel.model{iLevel}{jNode},tdmodel.yid{iLevel}{jNode});
    obsindcs = ~isnan(ytest(:,iLevel)) & obsindcs;
else
    return;
end

%% convergence criterium for recursion
if iLevel == length(tdmodel.y)
    return;
end

nodeids = unique(ytest(obsindcs,iLevel))';
for jNode = nodeids
    obsindcs  = ytest(:,iLevel)==jNode;
    [ytest,P, scoresX,scoresY] = topdownHCrecpred(Xtest, tdmodel, ytest, P, iLevel+1, jNode, obsindcs,scoresX,scoresY);
end
return;
end