function [RetrievedData] = RetrieveClassification

%%% This function takes in a 2-column matrix with genus (column 1) and species (column 2)
%%% and generates a 6 column matrix:
%%%
%%% Column 1 | Column 2 | Column 3 | Column 4 | Column 5 | Column 6 |
%%%---------------------------------------------------------------------
%%% Gram     | Class    | Order    | Family   | Genus    | Species  |
%%%
%%% Data is retrieved based on the genus.
%%% Make sure genus names are well typed and there are no mistakes.

%% Initialisation of POI Libs
% Add Java POI Libs to matlab javapath
javaaddpath('poi_library/poi-3.8-20120326.jar');
javaaddpath('poi_library/poi-ooxml-3.8-20120326.jar');
javaaddpath('poi_library/poi-ooxml-schemas-3.8-20120326.jar');
javaaddpath('poi_library/xmlbeans-2.3.0.jar');
javaaddpath('poi_library/dom4j-1.6.1.jar');
javaaddpath('poi_library/stax-api-1.0.1.jar');

% Ask the user to specify an Excel file...
[fileName,pathName] = uigetfile({'*.xlsx','*.xls'},'Select File');

% Error!
if length(fileName) < 4
    errMsg = ['No file selected.' char(10) ...
        'Cannot proceed without this file.' char(10) ...
        'Quitting!'];
    error(errMsg);
end

prompt = {'Enter spectrum file name column (e.g. A)','Enter genus and species columns (e.g. A:B):', 'Headers included? (Y/N):'};
dlg_title = 'Input Labels';
num_lines = 1;
default = {'A', 'B:C', 'Y'};
answer = inputdlg(prompt,dlg_title,num_lines,default);

xlRange = char(answer{2});
titleColumn = char([answer{1}, ':', answer{1}]);

[~,~,SpeciesLabels] = xlsread([pathName fileName], 1, xlRange);
[~,~,FileName] = xlsread([pathName fileName], 1, titleColumn);

%%Remove any NaNs from the data file
SpeciesLabels(any(cellfun(@(x) any(isnan(x)),SpeciesLabels),2),:) = [];

if strcmpi(answer{3}, 'y')
    SpeciesLabels = SpeciesLabels(2:end,:);
FileName = FileName(2:end,:);
else
end

%% remove any spaces before or after the labels
SpeciesLabels = strtrim(SpeciesLabels);
NotFoundCounter = 0;
idxBin = [];

%% concatenates the genus and the species in the species column
SpeciesLabels(:,2) = strcat(SpeciesLabels(:,1), {' '}, SpeciesLabels(:,2));
RetrievedData = cell(size(SpeciesLabels,1), 6);

%% gram data retrieval databases
gramURL = ['http://www.nmpdr.org/FIG/wiki/view.cgi/FIG/GramStain'];
gramPosURL = ['http://www.globalrph.com/bacterial-strains-gram-positive.htm'];
gramNegURL = ['http://www.globalrph.com/bacterial-strains-gram-negative.htm'];
gramNegURL2 = ['http://www.globalrph.com/Klebsiella-species.htm'];

gramURL = char(gramURL);
gramRetrieved = urlread(gramURL);
initials = {'ac', 'dl', 'mr', 'sz'};
h=waitbar(0,'Please wait...');

%% retrieve unique species and genera
uniqueSpecies = unique(SpeciesLabels(:,2), 'stable');
uniqueGenera = unique(SpeciesLabels(:,1),'stable');

%% loop data retrieval for unique species

for n = 1:length(uniqueSpecies)
    idx = find(strcmpi(SpeciesLabels(:,2), uniqueSpecies(n)));
    element = SpeciesLabels(idx,1);
    element = element(1);
    waitbar(n/length(uniqueSpecies),h, sprintf('Retrieving information for ''%s'' (%d of %d)', element{:}, n, length(uniqueSpecies)));
    Conversion = char(element);
    %% get first genus letter
    firstChar = Conversion(1);
    charToNum = double(firstChar);
    
    switch charToNum
        case num2cell(65:67)
            init = initials{1};
        case num2cell(68:76)
            init = initials{2};
        case num2cell(77:82)
            init = initials{3};
        case num2cell(83:90)
            init = initials{4};
    end
    
    api = 'http://www.bacterio.net/-classification';
    url = [api init '.html'];
    gramURL = char(url);
    S = urlread(gramURL);
    startStringSearch = '">';
    endStringSearch = '</a>';
    altStartStringSearch = '&quot;<i>';
    altEndStringSearch = '</i>&quot;';
    fullName = strcat(element);
    
    elementidx = findstr(char(fullName),S);
    lastMention = elementidx(1,end);
    ReducedS = S(lastMention:end);
    
    Classidx = findstr('Class', ReducedS);
    Classidx = Classidx(1);
    if strcmp(ReducedS(Classidx+7), '&')
        endStringSearch = altEndStringSearch;
        startStringSearch = altStartStringSearch;
    else
        startStringSearch = '">';
        endStringSearch = '</a>';
    end
    Endidx = findstr(endStringSearch, ReducedS(Classidx:end));
    Endidx = Endidx(1) + Classidx - 2;
    Startidx = findstr(startStringSearch, ReducedS(Classidx:end));
    Class = ReducedS((Startidx(1)+length(startStringSearch)+Classidx-1):Endidx(1));
    RetrievedData(idx, 2) = {Class};
    
    Orderidx = findstr('Order', ReducedS);
    Orderidx = Orderidx(1);
    if strcmp(ReducedS(Orderidx+7), '&')
        endStringSearch = altEndStringSearch;
        startStringSearch = altStartStringSearch;
    else
        startStringSearch = '">';
        endStringSearch = '</a>';
    end
    Endidx = findstr(endStringSearch, ReducedS(Orderidx:end));
    Endidx = Endidx(1) + Orderidx - 2;
    Startidx = findstr(startStringSearch, ReducedS(Orderidx:end));
    Order = ReducedS((Startidx(1)+length(startStringSearch)+Orderidx-1):Endidx(1));
    RetrievedData(idx, 3) = {Order};
    
    
    Familyidx = findstr('Family',ReducedS);
    Familyidx = Familyidx(1);
    if strcmp(ReducedS(Orderidx+8), '&')
        endStringSearch = altEndStringSearch;
        startStringSearch = altStartStringSearch;
    else
        startStringSearch = '">';
        endStringSearch = '</a>';
    end
    Endidx = findstr(endStringSearch, ReducedS(Familyidx:end));
    Endidx = Endidx(1) + Familyidx -2;
    Startidx = findstr(startStringSearch, ReducedS(Familyidx:end));
    Family = ReducedS((Startidx(1)+length(startStringSearch)+Familyidx-1):Endidx(1));
    RetrievedData(idx, 4) = {Family};
    
    RetrievedData(idx, 5) = {element};
    RetrievedData(idx, 6) = {uniqueSpecies(n)};
    
    Conversion = char(element);
    ElementIDX = findstr(Conversion, gramRetrieved);
    
    Positive = findstr('SHOW POSITIVE', gramRetrieved);
    Negative = findstr('SHOW NEGATIVE', gramRetrieved);
    
    if isempty(ElementIDX)
        S = urlread(gramPosURL);
        elementidx = findstr(char(fullName),S);
        if isempty(elementidx)
            S = urlread(gramNegURL);
            elementidx = findstr(char(fullName),S);
            if isempty(elementidx)
                S = urlread(gramNegURL2);
                elementidx = findstr(char(fullName),S);
                if isempty(elementidx)
                    RetrievedData(idx,1) = {''};
                    NotFoundCounter = NotFoundCounter + 1;
                    idxBin = [idxBin; idx];
                else
                    Gram = '-';
                    RetrievedData(idx,1) = {Gram};
                end
            else
                Gram = '-';
                RetrievedData(idx,1) = {Gram};
            end
        else
            Gram = '+';
            RetrievedData(idx,1) = {Gram};
        end
        
    else
        if ElementIDX(1) < Positive
            Gram = '+';
        elseif ElementIDX(1) > Positive && ElementIDX(1) < Negative
            Gram = '-';
        end
        RetrievedData(idx,1) = {Gram};
    end
end

Classes = RetrievedData(idxBin,2);
uniqueClasses = unique(Classes);

for nMissing = 1:length(uniqueClasses)
    otherEqualClassesIdx = find(strcmp(RetrievedData(:,2), uniqueClasses(nMissing)));
    %% remove idx that are already missing
    otherEqualClassesIdxExc = setdiff(otherEqualClassesIdx, idxBin);
    GramLabel = RetrievedData{otherEqualClassesIdxExc(1)};
    MissingIdx = setdiff(otherEqualClassesIdx, otherEqualClassesIdxExc);
            
    if length(otherEqualClassesIdx) > 0
        RetrievedData(MissingIdx,1) = {GramLabel};
    else
        RetrievedData(MissingIdx,1) = {''};
    end
        
end
close(h);
if NotFoundCounter > 0
    msgbox(sprintf('%i entries not found!',NotFoundCounter), 'Error', 'error')
else
end

header = {'Gram', 'Class', 'Order', 'Family', 'Genus', 'Species'};
FileNameHeader = {'Filename'};
LabeledRetrievedData = [header; RetrievedData];
FileName = [FileNameHeader; FileName];
LabeledRetrievedData = [FileName, LabeledRetrievedData];

xlwrite('RetrievedClassData.xlsx', LabeledRetrievedData, 'Sheet1', 'A1');
end






