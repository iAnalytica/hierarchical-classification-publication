# README #

Repository consists of 3 main directories:

* Data
* Retrieve_Taxonomy
* Classification_Prediction


### 1. Data ###

2 main datasets are provided:
* bacterial mass spectrometric data; and
* TCGA mRNA sequencing data. 

Each dataset contains the X matrix (quantitative data) and the metadata/labels for classification/prediction. The TCGA clusters extracted from literature at the time of publication are also provided in the original format and matrix format. Bacterial hierarchical taxonomy was extracted using the "retrieve metadata" module (see below).


----------------------------------------------------------------------

### 2. Retrieve taxonomy ###

This function facilitates retrieval of bacterial taxonomies. This takes an excel file (.xlsx or .xls) which is a 2-column matrix with genus (column 1) and species (column 2) and generates a 6 column matrix as follows:

Column 1 | Column 2 | Column 3 | Column 4 | Column 5 | Column 6 |
--------------------------------------------------------------------
Gram     | Class    | Order    | Family   | Genus    | Species  |

2 main sources are used to retrieve the data:

* http://www.nmpdr.org for gram staining information
* http://www.globalrph.com for taxonomic information


----------------------------------------------------------------------

### 3. Hierarchical classification and Leave-one-class-out prediction ###

This directory contains the main classification and prediction algorithms. It is subdivided into 4 sub-directories:

* DRMethods - contains the supervised and unsupervised dimensionality reduction and classification methods including: SVM, PLS, MMCLDA, and PCALDA.

* Hierarchical_classification - contains the main algorithm for performing hierarchical classification. To run hierarchical classification, call the function 'HierClassification.m' as follows:

HierClassification(X, Y)

where X is the quantitative matrix, and Y is the hierarchical label matrix (see function header for further details). Make sure that the DRMethods and visualizations modules are added to the path/running environment.

On calling the function, you will be prompted with how many cross-validation rounds and how many number of prediction repetitions the algorithm should run for. As output, during runtime this generates a biograph to mark the progress of the classification at each node of the hierarchical tree, and when finishes generates a figure with the confusion matrix for the bottom-most level with the micro-average and range of classification accuracies across repetitions, jpeg version for each confusion matrix for each level of the hierarchy, and txt files with the raw accuracies (average, maximum, minimum and standard deviation).

* Leave_one_out_prediction - contains the main algorithm for performing leave-one-class-out prediction. Call the function 'topdownHCleaveout.m' as follows:

topdownHCleaveout(X, Y, nreps)

where X is the quantitative matrix, and Y is the hierarchical label matrix and nreps is the number of repetitions to perform the predictions of the left-out class (see function header for further details). This can be the same used for the hierarchical classification. Make sure the DRMethods and visualizations modules are added to the path/running environment.

On calling the function, you will be prompted with which genus/species to leave out and predict. As output, this generates a biograph to mark the progress of the prediction at each node of the hierarchical tree, a biograph at the end for each prediction to show the selected method at each node on mouse hover, and cross-validation align score plots for the classification method at each node indicating which samples were predicted to the right class, misclassified, or not classified at all (i.e. the probability difference between the top classes does not exceed the threshold). A jpeg image with a 2D discriminant plot is generated for each hierarchy level for each repetition for the left out class.

* Visualizations - contains the functions to plot the confusion matrices, generate distinguishable colors for a colorbar on the right of confusion matrices to indicate the previous hierarchy level classes, and for the dynamic updating of the biograph indicators during the running of the algorithms. When adopted from other authors, the original authors have been cited.

��----------------------------------------------------------------------

